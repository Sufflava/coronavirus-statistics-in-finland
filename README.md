# Coronavirus statistics in Finland.

This website is made for my personal use, it is not published anywhere. Data that is used on the web site is updated a couple of times in a week, so you can download the source code and run this web site by yourself on your computer and see my latest update.

Source code: [On Bitbucket](https://bitbucket.org/Sufflava/coronavirus-statistics-in-finland/src/master/web-site/)

Data: [Saved examples of files](https://bitbucket.org/Sufflava/coronavirus-statistics-in-finland/src/master/data/)

Supported languages: English, Russian

All the numbers, that are shown on this website, are taken from the official THL's website (National Institute for Health and Welfare of Finland) and offician THL's API.

## Installation

If you want to run the web site and see the charts in dynamic

1. Download the code from [web-site](https://bitbucket.org/Sufflava/coronavirus-statistics-in-finland/src/master/web-site/) folder

2. In the root folder run the command

`npm install`

`npm start`

3. Open [http://localhost](http://localhost) in a browser of your choise.

## Statistics that is shown
- Current situation - summed up numbers (for the whole country, for all the time)
- Chart with daily dynamic (for the whole country)
- Current hostpitalisations - number of people that are currently hospitalised (for the whole country)
- Statistics by age
- Statistics by sex
- Chart with daily number of cases devided by regions
- Population by region (Map view)

![site_screenshot_1](https://bitbucket.org/Sufflava/coronavirus-statistics-in-finland/raw/bde54356ecb797949f365b3eed608acda7a8712c/images/site_screenshot_1.png)

![site_screenshot_2](https://bitbucket.org/Sufflava/coronavirus-statistics-in-finland/raw/bde54356ecb797949f365b3eed608acda7a8712c/images/site_screenshot_2.png)

![site_screenshot_3](https://bitbucket.org/Sufflava/coronavirus-statistics-in-finland/raw/bde54356ecb797949f365b3eed608acda7a8712c/images/site_screenshot_3.png)

![site_screenshot_4](https://bitbucket.org/Sufflava/coronavirus-statistics-in-finland/raw/bde54356ecb797949f365b3eed608acda7a8712c/images/site_screenshot_4.png)

![site_screenshot_5](https://bitbucket.org/Sufflava/coronavirus-statistics-in-finland/raw/bde54356ecb797949f365b3eed608acda7a8712c/images/site_screenshot_5.png)

![site_screenshot_6](https://bitbucket.org/Sufflava/coronavirus-statistics-in-finland/raw/bde54356ecb797949f365b3eed608acda7a8712c/images/site_screenshot_6.png)

![site_screenshot_7](https://bitbucket.org/Sufflava/coronavirus-statistics-in-finland/raw/bde54356ecb797949f365b3eed608acda7a8712c/images/site_screenshot_7.png)

![site_screenshot_8](https://bitbucket.org/Sufflava/coronavirus-statistics-in-finland/raw/a4f4c0ab9de6f845a96f89453b2f76be5ce0e82b/images/site_screenshot_8.png)

## API request calls and resources that were used for getting data:
- daily country wide statistics in English (number of tests, number of positive cases, number of deaths)

[https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?column=dateweek2020010120201231-443702L&column=measure-141082](https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?column=dateweek2020010120201231-443702L&column=measure-141082)

- total numbers for current date are taken from weekly statistics

[https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082](https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082)

- number of hostpitalisations are taken from THL's web site

[https://thl.fi/en/web/infectious-diseases-and-vaccinations/what-s-new/coronavirus-covid-19-latest-updates/situation-update-on-coronavirus](https://thl.fi/en/web/infectious-diseases-and-vaccinations/what-s-new/coronavirus-covid-19-latest-updates/situation-update-on-coronavirus)

- statistics by age

Number of positive cases devided by age:

[https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?column=ttr10yage-444309](https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?column=ttr10yage-444309)

Number of deaths devided by age:

[https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?column=measure-492118&column=ttr10yage-444309](https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?column=measure-492118&column=ttr10yage-444309)

- statistics by sex

Number of positive cases devided by sex:

[https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?column=sex-444328](https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?column=sex-444328)

Number of deaths devided by sex:

[https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?column=measure-492118&column=sex-444328](https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?column=measure-492118&column=sex-444328)

- statistics by region

Number of positive cases by region

[https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443702L&column=hcdmunicipality2020-445222](https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443702L&column=hcdmunicipality2020-445222)

Population by region

[https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-445344&column=hcdmunicipality2020-445222](https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-445344&column=hcdmunicipality2020-445222)


## Statistics that has not been found yet
- Daily dynamic of hospitalisations, country wide and devided by region (currently, only today's numbers are available)
- Daily number of tests that are done devided by age and sex
- Daily number of tests and deaths devided by region (currently only country wide numbers are available)

## Used libraries:
- [JSON-stat Javascript Toolkit](https://www.npmjs.com/package/jsonstat-toolkit)
- [Recharts](https://recharts.org/en-US/)
- [d3.js](https://d3js.org/)
- [schemecolor](https://www.schemecolor.com/stunning-pie-chart-color-scheme.php)
- [momentjs](https://momentjs.com/)
- [Turf](http://turfjs.org/)
- [Papa Parse](https://www.papaparse.com/)
- [React](https://reactjs.org/)
- [Redux](https://redux.js.org/)
- [react localize redux](https://ryandrewjohnson.github.io/react-localize-redux-docs/)


## Map of Finland

Map of Finland with SHP regions in geojson format was downloaded from here: [sairaanhoitopiirit.geojson](https://media-koronatilanne.hub.arcgis.com/datasets/esrifinland::sairaanhoitopiirit/geoservice)


## Contributors
 - [Lasicaine](https://github.com/Lasicaine) (Design & usability)