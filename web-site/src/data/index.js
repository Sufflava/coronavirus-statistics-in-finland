export { default as labels } from './statistics/labels';
export { default as lastDayOfWeek } from './statistics/last-day-of-week';
export { default as hcdRegions } from './hcd-regions';
export { default as hcdRegionsMapGeoJson } from './hcd-regions';

// https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?column=dateweek2020010120201231-443702L&column=measure-141082
export { default as allMeasuresDaily } from './statistics/all-measures-daily';

// https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082
export { default as allMeasuresWeekly } from './statistics/all-measures-weekly';

// https://thl.fi/en/web/infectious-diseases-and-vaccinations/what-s-new/coronavirus-covid-19-latest-updates/situation-update-on-coronavirus
export { default as additionalStatistics } from './statistics/additional-statistics';

// https://pxnet2.stat.fi/PXWeb/pxweb/en/Kokeelliset_tilastot/Kokeelliset_tilastot__vamuu_koke/koeti_vamuu_pxt_12ng.px/table/tableViewLayout2/
// https://pxnet2.stat.fi/PXWeb/pxweb/en/Kokeelliset_tilastot/Kokeelliset_tilastot__vamuu_koke/koeti_vamuu_pxt_12ng.px/
export { default as rawNumberOfAllDeaths } from './statistics/number-of-deaths-all-reasons-weekly';

// https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?column=ttr10yage-444309
export { default as allPositiveCasesByAge } from './statistics/number-of-cases-by-age-all';

// https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?column=measure-492118&column=ttr10yage-444309
export { default as allDeathsByAge } from './statistics/number-of-deaths-by-age-all';

// https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?column=sex-444328
export { default as allPositiveCasesBySex } from './statistics/number-of-cases-by-sex-all';

// https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?column=measure-492118&column=sex-444328
export { default as allDeathsBySex } from './statistics/number-of-deaths-by-sex-all';

// https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443702L&column=hcdmunicipality2020-445222
export { default as positiveCasesByRegionDaily } from './statistics/number-of-cases-by-region-daily';

// https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-445344&column=hcdmunicipality2020-445222
export { default as populationByRegionWeekly } from './statistics/population-by-region-weekly';

// currently not shown
// https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-445356&column=hcdmunicipality2020-445222
export { default as testsByRegionWeekly } from './statistics/number-of-tests-by-region-weekly';

// currently not shown
// https://sampo.thl.fi/pivot/prod/en/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=hcdmunicipality2020-445222
export { default as positiveCasesByRegionWeekly } from './statistics/number-of-cases-by-region-weekly';

/*

[{"444996": "hcd19"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-444996
"Pohjois-Pohjanmaan SHP"
File: all-measures-hcd19-weekly-fi.json

{"445014": "hcd7"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445014
"Päijät-Hämeen SHP"
File: all-measures-hcd7-weekly-fi.json

{"445043": "hcd9"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445043
"Etelä-Karjalan SHP"
File: all-measures-hcd9-weekly-fi.json

{"445079": "hcd16"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445079
"Vaasan SHP"
File: all-measures-hcd16-weekly-fi.json

{"445101": "hcd20"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445101
"Kainuun SHP"
File: all-measures-hcd20-weekly-fi.json

{"445131": "hcd2"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445131
"Ahvenanmaa"
File: all-measures-hcd2-weekly-fi.json

{"445155": "hcd10"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445155
"Etelä-Savon SHP"
File: all-measures-hcd10-weekly-fi.json

{"445170": "hcd4"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445170
"Satakunnan SHP"
File: all-measures-hcd4-weekly-fi.json

{"445175": "hcd11"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445175
"Itä-Savon SHP"
File: all-measures-hcd11-weekly-fi.json

{"445178": "hcd8"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445178
"Kymenlaakson SHP"
File: all-measures-hcd8-weekly-fi.json

{"445190": "hcd21"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445190
"Länsi-Pohjan SHP"
File: all-measures-hcd21-weekly-fi.json

{"445193": "hcd23"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445193
"Helsingin ja Uudenmaan SHP"
File: all-measures-hcd23-weekly-fi.json

{"445197": "hcd3"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445197
"Varsinais-Suomen SHP"
File: all-measures-hcd3-weekly-fi.json

{"445206": "hcd5"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445206
"Kanta-Hämeen SHP"
File: all-measures-hcd5-weekly-fi.json

{"445222": "hcd1"}, // all

{"445223": "hcd13"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445223
"Pohjois-Savon SHP"
File: all-measures-hcd13-weekly-fi.json

{"445224": "hcd22"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445224
"Lapin SHP"
File: all-measures-hcd22-weekly-fi.json

{"445225": "hcd15"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445225
"Etelä-Pohjanmaan SHP"
File: all-measures-hcd15-weekly-fi.json

{"445230": "hcd17"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445230
"Keski-Pohjanmaan SHP"
File: all-measures-hcd17-weekly-fi.json

{"445282": "hcd6"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445282
"Pirkanmaan SHP"
File: all-measures-hcd6-weekly-fi.json

{"445285": "hcd14"},
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445285
"Keski-Suomen SHP"
File: all-measures-hcd14-weekly-fi.json

{"445293": "hcd12"}]
https://sampo.thl.fi/pivot/prod/fi/epirapo/covid19case/fact_epirapo_covid19case.json?row=dateweek2020010120201231-443686&column=measure-141082&column=hcdmunicipality2020-445293
"Pohjois-Karjalan SHP"
File: all-measures-hcd12-weekly-fi.json

*/