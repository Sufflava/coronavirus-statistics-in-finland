import { getTranslate, getActiveLanguage } from 'react-localize-redux';
import { bindActionCreators } from 'redux';
import { statisticsActions } from '../actions';

export const mapStateToProps = state => {
    return {
        translate: getTranslate(state.localize),
        currentLanguage: getActiveLanguage(state.localize).code,
        weeklyRegionalStatistics: state.statistics.weeklyRegionalStatistics,
        dailyRegionalStatistics: state.statistics.dailyRegionalStatistics,
        regionalTotals: state.statistics.regionalTotals
    };
};

export const mapDispatchToProps = (dispatch) => {
    return {
        statisticsActions: bindActionCreators(statisticsActions, dispatch)
    };
};