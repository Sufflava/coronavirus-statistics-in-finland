import { getTranslate, getActiveLanguage } from 'react-localize-redux';
import { bindActionCreators } from 'redux';
import { statisticsActions } from '../actions';

export const mapStateToProps = state => {
    return {
        translate: getTranslate(state.localize),
        currentLanguage: getActiveLanguage(state.localize).code,
        statisticsBySex: state.statistics.statisticsBySex
    };
};

export const mapDispatchToProps = (dispatch) => {
    return {
        statisticsActions: bindActionCreators(statisticsActions, dispatch)
    };
};