import React from 'react';
import { render } from "react-dom";
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './reducers/rootReducer';
import { LocalizeProvider } from "react-localize-redux";
import App from './App';
import 'fontsource-roboto';
import './index.css';
//import reportWebVitals from './reportWebVitals';

const store = createStore(rootReducer);

render(
    <Provider store={store}>
        <LocalizeProvider store={store}>
            <App />
        </LocalizeProvider>
    </Provider>, 
    document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
//reportWebVitals();
