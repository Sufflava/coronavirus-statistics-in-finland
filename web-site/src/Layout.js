import React from "react";
import { Translate } from "react-localize-redux";
import clsx from 'clsx';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import { 
    AllMeasuresChart, CurrentSituationPanel, LanguageToggle, 
    HospitalisationsChart, StatisticsByAgeChart, StatisticsBySexChart,
    DailyRegionalStatisticsChart, WeeklyRegionalStatisticsChart, 
    CurrentRegionalPopulationMap
} from './components';
import './components/style/ChartLegend.css';
import './components/style/ChartTooltip.css';
import './components/style/RegionalMap.css';

const footerItems = [
    {
        title: 'layout.about',
        links: [
        {
            url: "https://github.com/Sufflavus/coronavirus-in-finland",
            text: "layout.sourceCode"
        }, 
        {
            url: "https://bitbucket.org/Sufflava/coronavirus-statistics-in-finland/src/master/data/",
            text: "layout.usedData"
        }, 
        {
            url: "https://sufflavus.github.io/",
            text: "layout.developerWebSite"
        }]
    },
    {
        title: 'layout.officialResources',
        links: [
        {
            url: "https://thl.fi/en/web/infectious-diseases-and-vaccinations/what-s-new/coronavirus-covid-19-latest-updates/situation-update-on-coronavirus",
            text: "layout.thlLastUpdate"
        }, 
        {
            url: "https://experience.arcgis.com/experience/d40b2aaf08be4b9c8ec38de30b714f26",
            text: "layout.statisticsOnMap"
        }, 
        {
            url: "https://thl.fi/en/web/thlfi-en/statistics/statistical-databases/open-data/confirmed-corona-cases-in-finland-covid-19-",
            text: "layout.thlOpenDataApi"
        },
        {
            url: "http://www.stat.fi/uutinen/statistics-finland-will-release-exceptional-rapid-estimate-statistics-on-the-number-of-deaths",
            text: "layout.statisticsFinland"
        }]
    }
];

function Footer() {
    return (
        <Grid container spacing={4} justify="space-evenly">
            {footerItems.map((footer) => (
                <Grid item xs={12} sm={6} key={footer.title}>
                    <Typography variant="h6" color="textPrimary" gutterBottom>
                        <Translate id={footer.title} />
                    </Typography>
                    <ul>
                        {footer.links.map((item) => (
                            <li key={item.text}>
                                <Link href={item.url} target="_blank" rel="noreferrer" variant="subtitle1" color="textSecondary">
                                    <Translate id={item.text} />
                                </Link>
                            </li>
                        ))}
                    </ul>
                </Grid>
            ))}
        </Grid>
    );
}

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            <Translate id="layout.copyright" />{' '}
            <Link color="inherit" href="https://sufflavus.github.io/" target="_blank" rel="noreferrer">
                Sufflavus
            </Link>{' '}
            {new Date().getFullYear()}
        </Typography>
    );
}

const useStyles = theme => ({
    '@global': {
      ul: {
        margin: 0,
        padding: 0,
        listStyle: 'none',
      },
    },
    appBar: {
      backgroundColor: theme.palette.white,
      borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbar: {
      flexWrap: 'wrap',
    },
    toolbarTitle: {
      flexGrow: 1,
    },
    link: {
      margin: theme.spacing(1, 1.5),
    },
    heroContent: {
      padding: theme.spacing(8, 0, 6),
    },
    cardHeader: {
      backgroundColor:
        theme.palette.type === 'light' ? theme.palette.grey[200] : theme.palette.grey[700],
    },
    cardPricing: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'baseline',
      marginBottom: theme.spacing(2),
    },
    panel: {
      marginBottom: theme.spacing(5),
    },
    footer: {
      borderTop: `1px solid ${theme.palette.divider}`,
      marginTop: theme.spacing(8),
      paddingTop: theme.spacing(3),
      paddingBottom: theme.spacing(3),
      [theme.breakpoints.up('sm')]: {
        paddingTop: theme.spacing(6),
        paddingBottom: theme.spacing(6),
      },
    },
  });

class Layout extends React.Component {
    render() {
        const { classes } = this.props;
        const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);

        return (
            <React.Fragment>
                <CssBaseline />
                <AppBar position="static" color="default" elevation={0} className={classes.appBar}>
                    <Toolbar className={classes.toolbar}>
                        <Typography variant="h6" color="inherit" noWrap className={classes.toolbarTitle}>
                            <Translate id="layout.siteName" />
                        </Typography>
                        <LanguageToggle/>
                    </Toolbar>
                </AppBar>
                {/* Hero unit */}
                <Container maxWidth="md" component="main" className={classes.heroContent}>
                    <Box p={3}>
                        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                            <Translate id="layout.pageTitle" />
                        </Typography>
                        <Box pb={2}>
                            <Typography align="left" color="textSecondary" component="p">
                                <Translate id="layout.pageAbout" />
                            </Typography>
                        </Box>
                        <Box pb={2}>
                            <Typography align="left" color="textSecondary" component="p">
                                <Translate id="layout.officialDataInfo" 
                                    data={{ 
                                        thlLastUpdateLink: <Link href="https://thl.fi/en/web/infectious-diseases-and-vaccinations/what-s-new/coronavirus-covid-19-latest-updates/situation-update-on-coronavirus" target="_blank" rel="noreferrer"><Translate id="layout.here" /></Link>,
                                        thlMapLink: <Link href="https://experience.arcgis.com/experience/d40b2aaf08be4b9c8ec38de30b714f26" target="_blank" rel="noreferrer"><Translate id="layout.here" /></Link>,
                                        statisticsFinlandLink: <Link href="http://www.stat.fi/uutinen/statistics-finland-will-release-exceptional-rapid-estimate-statistics-on-the-number-of-deaths" target="_blank" rel="noreferrer"><Translate id="layout.here" /></Link>
                                    }} 
                                />
                            </Typography>
                        </Box>
                        <Typography align="left" color="textSecondary" component="p">
                            <Translate id="layout.dataConsistansyInfo" />
                        </Typography>
                    </Box>
                </Container>
                {/* End hero unit */}
                <Container maxWidth="md" component="main">
                    <Grid container spacing={3}>
                        <Grid item xs={12} className={classes.panel}>
                            <CurrentSituationPanel className={classes.paper}/>
                        </Grid>
                        <Grid item xs={12} className={classes.panel}>
                            <AllMeasuresChart className={fixedHeightPaper}/>
                        </Grid>
                        <Grid item xs={12} className={classes.panel}>
                            <HospitalisationsChart className={fixedHeightPaper}/>
                        </Grid>
                        <Grid item xs={12} className={classes.panel}>
                            <StatisticsByAgeChart className={fixedHeightPaper}/>
                        </Grid>
                        <Grid item xs={12} className={classes.panel}>
                            <StatisticsBySexChart className={fixedHeightPaper}/>
                        </Grid>
                        <Grid item xs={12} className={classes.panel}>
                            <DailyRegionalStatisticsChart className={fixedHeightPaper}/>
                        </Grid>
                        <Grid item xs={12} className={classes.panel}>
                            <WeeklyRegionalStatisticsChart className={fixedHeightPaper}/>
                        </Grid>
                        <Grid item xs={12} className={classes.panel}>
                            <CurrentRegionalPopulationMap className={fixedHeightPaper}/>
                        </Grid>
                    </Grid>
                </Container>
                {/* Footer */}
                <Container maxWidth="md" component="footer" className={classes.footer}>
                    <Footer/>
                    <Box mt={5}>
                        <Copyright />
                    </Box>
                </Container>
                {/* End footer */}
            </React.Fragment>
        );
    }
}

export default withStyles(useStyles)(Layout);