import moment from 'moment';
import 'moment/locale/ru';
import 'moment/locale/en-gb';

export function groupBy(items, key) {
    return items.reduce(
        (result, item) => ({
            ...result,
            [item[key]]: [
                ...(result[item[key]] || []),
                item,
            ],
        }), {});
}

export function formatDate(date, locale, format) {
    return moment(new Date(date)).locale(locale).format(format);
}

export function formatNumber(number, locale) { 
    return number.toLocaleString(locale);
}

// weekLabel is in format: "Year 2020 Week 03"
// return: "03"
export function getWeekNumber(weekLabel) {
    const parts = weekLabel.split(" ");
    return parts.length ? parts[parts.length - 1] : "";
}

// weekLabel is in format: "Year 2020 Week 03"
// return: "2020"
export function getWeekYear(weekLabel) {
    const parts = weekLabel.split(" ");
    return parts.length > 1 ? parts[1] : "";
}

// weekLabel is in format: "Year 2020 Week 03"
// returns: {year: "2020", week: "03"}
export function getWeekInfo(weekLabel) {
    const weekRegexp = /Year (?<year>[0-9]{4}) Week (?<week>[0-9]{2})/;
    const groups = weekLabel.match(weekRegexp).groups;
    return {year: groups.year, week: groups.week};
}

// weekLabel is in format: "2020W03*
// returns: {year: "2020", week: "03"}
export function getStatisticsWeekInfo(weekLabel) {
    const weekRegexp = /(?<year>[0-9]{4})W(?<week>[0-9]{2})*/;
    const groups = weekLabel.match(weekRegexp).groups;
    return {year: groups.year, week: groups.week};
}