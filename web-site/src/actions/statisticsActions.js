import JSONstat from "jsonstat-toolkit";
import moment from 'moment';
import * as Papa from 'papaparse';
import { 
    FETCH_COUNTRY_STATISTICS, FETCH_COUNTRY_TOTALS, FETCH_HOSPITALISATIONS, 
    FETCH_STATISTICS_BY_AGE, FETCH_STATISTICS_BY_SEX, FETCH_WEEKLY_REGIONAL_STATISTICS,
    FETCH_DAILY_REGIONAL_STATISTICS, FETCH_REGIONAL_TOTALS
} from './actionTypes';
import { 
    allMeasuresDaily, allMeasuresWeekly, additionalStatistics, allPositiveCasesByAge, 
    allDeathsByAge, allPositiveCasesBySex, allDeathsBySex, 
    testsByRegionWeekly, positiveCasesByRegionWeekly, positiveCasesByRegionDaily,
    populationByRegionWeekly, labels, lastDayOfWeek, rawNumberOfAllDeaths
} from '../data';
import {
    POPULATION_MEASURE_ID, NUMBER_OF_TESTS_MEASURE_ID, NUMBER_OF_POSITIVE_CASES_MEASURE_ID, 
    NUMBER_OF_DEATHS_MEASURE_ID, SEX_TRANSLATION_MAP, ALL_WEEKS_MEASURE_ID,
    REGION_MEASURES_TO_GEO_AREA_MAP
} from '../constants';
import {groupBy, getWeekInfo as getThlWeekInfo, getStatisticsWeekInfo} from "../utils";

// show only number of tests, positive cases and deaths
const MEASURES = [
    NUMBER_OF_TESTS_MEASURE_ID, 
    NUMBER_OF_POSITIVE_CASES_MEASURE_ID, 
    NUMBER_OF_DEATHS_MEASURE_ID
];

// In weekly statistics it indicates total for all the time
const TIME_MEASURE_ID = "443686";

const DAY_PROP_NAME = "dateweek2020010120201231";
const WEEK_PROP_NAME = DAY_PROP_NAME;
const AGE_PROP_NAME = "ttr10yage";
const SEX_PROP_NAME = "sex";
const REGION_PROP_NAME = "hcdmunicipality2020";

// In statistics can be this placeholder instead of actual number.
// ".." means that actual number is unknown, but it is between 1 and 4 (inclusive).
const LESS_THEN_FIVE_PLACEHOLDER = "..";

function getCountryStatistics() {
    const J = JSONstat(allMeasuresDaily);
    const data = J.Dataset(0).toTable({ type : "arrobj", content: "id"});
    const dataToShow = data
        .filter(item => item.value != null && MEASURES.includes(item.measure))
        .map(item => {
            var date = labels.days[item[DAY_PROP_NAME]];
            return { 
                measure: item.measure,  
                value: +item.value,
                date: date
            };
        });    
    const groupsByDate = groupBy(dataToShow, "date");
    const resultData = [];
    Object.keys(groupsByDate).forEach(key => {
        const arr = groupsByDate[key];
        let obj = {date: key};
        arr.forEach(val => {
            obj[val.measure] = val.value;
        });
        resultData.push(obj);
    });

    return resultData;
}

function getTotalsFromWeeklyData() {
    const J = JSONstat(allMeasuresWeekly);
    const data = J.Dataset(0)
        .toTable({ type : "arrobj", content: "id" })
        .filter(x => x[DAY_PROP_NAME] === TIME_MEASURE_ID);

    let totals = {};
    
    data.forEach(x => {
        totals[x.measure] = +x.value;
    });
    
    return {
        population: totals[POPULATION_MEASURE_ID], 
		numberOfTests: totals[NUMBER_OF_TESTS_MEASURE_ID],
        numberOfPositiveCases: totals[NUMBER_OF_POSITIVE_CASES_MEASURE_ID],
        numberOfDeaths: totals[NUMBER_OF_DEATHS_MEASURE_ID],
    };
}

function getNumberOfAllDeaths() {
    const rawData = Papa.parse(rawNumberOfAllDeaths, {header: true});
    const data = rawData.data.map(item => ({
        weekInfo: getStatisticsWeekInfo(item["Week"]),
        value: +item["Total Total Deaths"]
    }));

    let total2020 = 0;
    data.forEach(item => {
        if(item.weekInfo.year === "2020") {
            total2020 += item.value;
        }
    });
    
    return {
        "2020": total2020
    };
}

function getCountryTotals() {
    const totals = getTotalsFromWeeklyData();
    const allDeaths = getNumberOfAllDeaths();
    //console.log(getNumberOfAllDeaths())
    //const allDeaths = additionalStatistics.allReasonsDeaths;

    return {
        ...additionalStatistics.allTimeTotals,
        ...totals,
        allReasonsDeaths: allDeaths
    }
}

function getHospitalisations() {
    return {
        areas: additionalStatistics.areas,
        hospitalisationsByArea: additionalStatistics.currentHospitalisationsByArea
    };
}

function getStatisticsByAge(dataJson) {
    const J = JSONstat(dataJson);
    const data = J.Dataset(0).toTable({ type : "arrobj", content: "id"});
    const dataToShow = data
        .map(item => {
            const measureId = item[AGE_PROP_NAME];
            const age = labels.ages[item[AGE_PROP_NAME]];
            const isLessThenFive = item.value === LESS_THEN_FIVE_PLACEHOLDER;
            return { 
                measureId,
                age,
                value: isLessThenFive ? 4 : +item.value, // if less then 5, put 4 to avoid out of type
                isLessThenFive: isLessThenFive
            };
        });
    return dataToShow;
}

function getStatisticsBySex(dataJson) {
    const J = JSONstat(dataJson);
    const data = J.Dataset(0).toTable({ type : "arrobj", content: "id"});
    const dataToShow = data
        .map(item => {
            const measureId = item[SEX_PROP_NAME];
            const labelTranslationId = SEX_TRANSLATION_MAP[measureId];

            return { 
                measureId,
                labelTranslationId,
                value: +item.value
            };
        });
    return dataToShow;
}

function getRawWeeklyRegionalStatistics(dataJson) {
    const J = JSONstat(dataJson);
    const data = J.Dataset(0).toTable({ type : "arrobj", content: "id"});
    const dataToShow = data
        .map(item => {
            const regionMeasureId = item[REGION_PROP_NAME];
            const weekId = item[WEEK_PROP_NAME];

            return { 
                weekId,
                regionMeasureId,
                value: +item.value
            };
        });
    return dataToShow;
}

function getWeeklyRegionalStatistics() {
    const numberOfTestsData = getRawWeeklyRegionalStatistics(testsByRegionWeekly);
    const numberOfPositiveCasesData = getRawWeeklyRegionalStatistics(positiveCasesByRegionWeekly);

    const todayDate = moment(new Date());
    const dateStart = {week: 6, year: 2020};
    const dateEnd = {week: +todayDate.format('w'), year: +todayDate.format('YYYY')};

    const data = [];

    Object.keys(labels.weeks).forEach(weekId => {
        if(weekId === ALL_WEEKS_MEASURE_ID) {
            return;
        }

        const weekLabel = labels.weeks[weekId];
        const weekInfo = getThlWeekInfo(weekLabel);
        const year = weekInfo.year;
        const weekNumber = weekInfo.week;

        if(+year <= dateStart.year && +weekNumber < dateStart.week) {
            return;
        }

        if(+year >= dateEnd.year && +weekNumber > dateEnd.week) {
            return;
        }

        const dateOfLastDayOfWeek = lastDayOfWeek.dates[year][weekNumber];

        Object.keys(labels.regions).forEach(regionMeasureId => {
            const numberOfTestsItem = numberOfTestsData
                .find(x => x.regionMeasureId === regionMeasureId && x.weekId === weekId);
            const numberOfPositiveCasesItem = numberOfPositiveCasesData
                .find(x => x.regionMeasureId === regionMeasureId && x.weekId === weekId);

            const regionName = labels.regions[regionMeasureId];

            const item = {
                weekId,
                weekLabel,
                dateOfLastDayOfWeek,
                year,
                weekNumber,
                regionMeasureId,
                regionName,
                [NUMBER_OF_TESTS_MEASURE_ID]: numberOfTestsItem ? numberOfTestsItem.value : undefined,
                [NUMBER_OF_POSITIVE_CASES_MEASURE_ID]: numberOfPositiveCasesItem ? numberOfPositiveCasesItem.value : undefined
            };

            data.push(item);
        });
    });

    data.sort((itemA, itemB) => {
        const yearA = +itemA.weekNumber;
        const yearB = +itemB.weekNumber;
        const weekA = +itemA.weekNumber;
        const weekB = +itemB.weekNumber;

        if(yearA < yearB) {
            return weekA - weekB;
        } else {
            return false;
        }
    });

    return {
        items: data,
        regions: labels.regions
    };
}

function getRawDailyRegionalStatistics(dataJson) {
    const J = JSONstat(dataJson);
    const data = J.Dataset(0).toTable({ type : "arrobj", content: "id"});
    const dataToShow = data
        .map(item => {
            const regionMeasureId = item[REGION_PROP_NAME];
            var date = labels.days[item[DAY_PROP_NAME]];

            return { 
                date,
                regionMeasureId,
                measureId: NUMBER_OF_POSITIVE_CASES_MEASURE_ID,
                value: +item.value
            };
        });
    return dataToShow;
}

function getDailyRegionalStatistics() {
    const numberOfPositiveCasesData = getRawDailyRegionalStatistics(positiveCasesByRegionDaily);

    const dateStart = moment("2020-01-15").valueOf();
    const dateEnd = moment(new Date()).valueOf();

    const resultData = [];

    numberOfPositiveCasesData.forEach(dataItem => {
        const date = moment(dataItem.date).valueOf();

        if(date < dateStart || date > dateEnd) {
            return;
        }

        const regionMeasureId = dataItem.regionMeasureId;
        const regionName = labels.regions[regionMeasureId];

        const item = {
            date: dataItem.date,
            regionMeasureId,
            regionName,
            [NUMBER_OF_POSITIVE_CASES_MEASURE_ID]: dataItem.value
        };

        resultData.push(item);
    });

    return {
        items: resultData,
        regions: labels.regions
    };
}

function getRegionalTotalsFromWeeklyData() {
    const populationData = getRawWeeklyRegionalStatistics(populationByRegionWeekly);
    const population = populationData
        .filter(item => item.weekId === ALL_WEEKS_MEASURE_ID)
        .map(item => ({
            ...item,
            regionName: labels.regions[item.regionMeasureId],
            hcdRegionId: REGION_MEASURES_TO_GEO_AREA_MAP[item.regionMeasureId]
        }));

    return {
        day: additionalStatistics.allTimeTotals.day,
        population
    };
}

function getRegionalTotals() {
    return getRegionalTotalsFromWeeklyData();
}

export function fetchCountryStatistics() {
    return { type: FETCH_COUNTRY_STATISTICS, data: getCountryStatistics() };
}

export function fetchCountryTotals() {
    return { type: FETCH_COUNTRY_TOTALS, data: getCountryTotals() };
}

export function fetchHospitalisations() {
    return { type: FETCH_HOSPITALISATIONS, data: getHospitalisations() };
}

export function fetchStatisticsByAge() {
    return { 
        type: FETCH_STATISTICS_BY_AGE, 
        data: {
            numberOfPositiveCases: getStatisticsByAge(allPositiveCasesByAge),
            numberOfDeaths: getStatisticsByAge(allDeathsByAge)
        }
    };
}

export function fetchStatisticsBySex() {
    return { 
        type: FETCH_STATISTICS_BY_SEX, 
        data: {
            numberOfPositiveCases: getStatisticsBySex(allPositiveCasesBySex),
            numberOfDeaths: getStatisticsBySex(allDeathsBySex)
        }
    };
}

export function fetchWeeklyRegionalStatistics() {
    return { 
        type: FETCH_WEEKLY_REGIONAL_STATISTICS, 
        data: getWeeklyRegionalStatistics()
    };
}

export function fetchDailyRegionalStatistics() {
    return { 
        type: FETCH_DAILY_REGIONAL_STATISTICS, 
        data: getDailyRegionalStatistics()
    };
}

export function fetchRegionalTotals() {
    return { 
        type: FETCH_REGIONAL_TOTALS, 
        data: getRegionalTotals()
    };
}