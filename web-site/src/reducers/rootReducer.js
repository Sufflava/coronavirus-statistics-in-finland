import { combineReducers } from 'redux';
import { localizeReducer } from "react-localize-redux";
import statisticsReducer from './statisticsReducer';

const rootReducer = combineReducers({
    localize: localizeReducer,
    statistics: statisticsReducer
});

export default rootReducer;