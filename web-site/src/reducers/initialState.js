export default {
    statistics: {
        countryStatistics: {
            items: [],
            loaded: false
        },
        countryTotals: {
            data: {},
            loaded: false
        },
        hospitalisations: {
            data: {},
            loaded: false
        },
        statisticsByAge: {
            data: {},
            loaded: false
        },
        statisticsBySex: {
            data: {},
            loaded: false
        },
        weeklyRegionalStatistics: {
            data: {},
            loaded: false
        },
        dailyRegionalStatistics: {
            data: {},
            loaded: false
        },
        regionalTotals: {
            data: {},
            loaded: false
        }
    }
};