import { combineReducers } from 'redux';
import { 
    FETCH_COUNTRY_STATISTICS, FETCH_COUNTRY_TOTALS, FETCH_HOSPITALISATIONS, 
    FETCH_STATISTICS_BY_AGE, FETCH_STATISTICS_BY_SEX, 
    FETCH_WEEKLY_REGIONAL_STATISTICS, FETCH_DAILY_REGIONAL_STATISTICS,
    FETCH_REGIONAL_TOTALS
} from '../actions';
import initialState from './initialState';

function countryStatistics(state = initialState.statistics.countryStatistics, action) {
    switch (action.type) {
        case FETCH_COUNTRY_STATISTICS:
            return {
                items: [...action.data],
                loaded: true
            };
        default:
            return state;
    }
}

function countryTotals(state = initialState.statistics.countryTotals, action) {
    switch (action.type) {
        case FETCH_COUNTRY_TOTALS:
            return {
                data: {...action.data},
                loaded: true
            };
        default:
            return state;
    }
}

function hospitalisations(state = initialState.statistics.hospitalisations, action) {
    switch (action.type) {
        case FETCH_HOSPITALISATIONS:
            return {
                data: {...action.data},
                loaded: true
            };
        default:
            return state;
    }
}

function statisticsByAge(state = initialState.statistics.statisticsByAge, action) {
    switch (action.type) {
        case FETCH_STATISTICS_BY_AGE:
            return {
                data: {...action.data},
                loaded: true
            };
        default:
            return state;
    }
}

function statisticsBySex(state = initialState.statistics.statisticsBySex, action) {
    switch (action.type) {
        case FETCH_STATISTICS_BY_SEX:
            return {
                data: {...action.data},
                loaded: true
            };
        default:
            return state;
    }
}

function weeklyRegionalStatistics(state = initialState.statistics.weeklyRegionalStatistics, action) {
    switch (action.type) {
        case FETCH_WEEKLY_REGIONAL_STATISTICS:
            return {
                data: {...action.data},
                loaded: true
            };
        default:
            return state;
    }
}

function dailyRegionalStatistics(state = initialState.statistics.dailyRegionalStatistics, action) {
    switch (action.type) {
        case FETCH_DAILY_REGIONAL_STATISTICS:
            return {
                data: {...action.data},
                loaded: true
            };
        default:
            return state;
    }
}

function regionalTotals(state = initialState.statistics.regionalTotals, action) {
    switch (action.type) {
        case FETCH_REGIONAL_TOTALS:
            return {
                data: {...action.data},
                loaded: true
            };
        default:
            return state;
    }
}

const statisticsReducer = combineReducers({
    countryStatistics,
    countryTotals,
    hospitalisations,
    statisticsByAge,
    statisticsBySex,
    weeklyRegionalStatistics,
    dailyRegionalStatistics,
    regionalTotals
});

export default statisticsReducer;