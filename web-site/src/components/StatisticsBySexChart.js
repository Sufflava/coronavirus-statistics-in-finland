import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Translate } from "react-localize-redux";
import Paper from '@material-ui/core/Paper'
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import {
    BarChart, ResponsiveContainer, XAxis, YAxis, CartesianGrid, 
    Tooltip, Legend, Bar, LabelList
} from 'recharts';
import {StatisticsBySexContainer as Container} from '../containers';
import { formatNumber } from '../utils';
import {PALETTE, ALL_SEXES_MEASURE_ID} from '../constants';
import ChartTitle from './ChartTitle';
import { default as CustomTooltip } from './BarChartTooltip';

const DEFAULT_BAR_OPACITY = 0.8;
const POSITIVE_CASES_MEASURE_ID = "numberOfPositiveCases";
const DEATHS_MEASURE_ID = "numberOfDeaths";

const MEASURE_ACTUAL_VALUES_MAP = {
    [POSITIVE_CASES_MEASURE_ID]: "casesLabel",
    [DEATHS_MEASURE_ID]: "deathsLAbel"
};

const MEASURE_LABEL_TRANSLATION_KEY_MAP = {
    [POSITIVE_CASES_MEASURE_ID]: "statistics.numberOfPositiveCases",
    [DEATHS_MEASURE_ID]: "statistics.numberOfDeaths"
};

const MEASURE_COLOR_MAP = {
    [POSITIVE_CASES_MEASURE_ID]: PALETTE.COLOR_3,
    [DEATHS_MEASURE_ID]: PALETTE.COLOR_6
};

class StatisticsBySexChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            opacity: {
                [POSITIVE_CASES_MEASURE_ID]: DEFAULT_BAR_OPACITY,
                [DEATHS_MEASURE_ID]: DEFAULT_BAR_OPACITY
            },
            visibleMeasures: [
                POSITIVE_CASES_MEASURE_ID, 
                DEATHS_MEASURE_ID
            ],
            allSexesVisible: false
        };
    }

    componentDidMount() {
        if(!this.props.statisticsBySex.loaded) {
            this.props.statisticsActions.fetchStatisticsBySex();
        }
    }

    _formatYAxis = (tickItem) => { 
        return formatNumber(tickItem, this.props.currentLanguage);
    }

    _formatTooltipItem = (value, name, props) => {
        const {casesLabel, deathsLabel} = props.payload;
        const realValue = name === POSITIVE_CASES_MEASURE_ID ? casesLabel : deathsLabel;
        
        const translationKey = MEASURE_LABEL_TRANSLATION_KEY_MAP[name]; 
        const label = this.props.translate(translationKey);
        
        return [
            realValue, 
            label
        ];
    }

    _onLegendClick = (e) => {
        const measureId = e.dataKey;
        this._toggleMeasure(measureId);
    }

    _onMeasureCheckBoxChange = (event) => {
        const measureId = event.target.name;
        this._toggleMeasure(measureId);      
    }

    _toggleMeasure = (measureId) => {
        const visibleMeasures = this.state.visibleMeasures;

        this.setState({
            visibleMeasures: visibleMeasures.includes(measureId) ?
                visibleMeasures.filter(x => x !== measureId) :
                [...visibleMeasures, measureId]
        });
    }

    _formatLegendItem = (value, entry) => {
        const translationKey = MEASURE_LABEL_TRANSLATION_KEY_MAP[value]; 
        return <Translate id={translationKey} />;
    }

    _onLegendMouseEnter = (o) => {
        const { dataKey } = o;
        const { opacity } = this.state;
    
  	    this.setState({
    	    opacity: { ...opacity, [dataKey]: 1 }
        });
    }

    _onLegendMouseLeave = (o) => {
        const { dataKey } = o;
        const { opacity } = this.state;
    
  	    this.setState({
    	    opacity: { 
                ...opacity, 
                [dataKey]: DEFAULT_BAR_OPACITY 
            }
        });
    }

    _onAllSexesCheckBoxChange = (event) => {
        this.setState({
    	    allSexesVisible: event.target.checked
        });
    }

    _renderFilters = () => {
        const { visibleMeasures, allSexesVisible } = this.state;

        return (
            <Box pl={2} pr={2} pb={2}>
                <Grid container direction="row" justify="center" alignItems="baseline">
                    {[POSITIVE_CASES_MEASURE_ID, DEATHS_MEASURE_ID].map(id => (
                        <Grid key={id} item xs={6} sm={3}>
                            <FormControlLabel
                                control={
                                    <Checkbox 
                                        name={id}
                                        checked={visibleMeasures.includes(id)} 
                                        onChange={this._onMeasureCheckBoxChange} 
                                        style ={{
                                            color: MEASURE_COLOR_MAP[id]
                                        }}
                                    />
                                }
                                label={
                                    <Typography variant="body2" style={{ color: MEASURE_COLOR_MAP[id] }}>
                                        <Translate id={MEASURE_LABEL_TRANSLATION_KEY_MAP[id]} />
                                    </Typography>     
                                }
                            />
                        </Grid>
                    ))}
                    <Grid key={"showAllSexes"} item xs={6} sm={3}>
                        <FormControlLabel
                            control={
                                <Checkbox 
                                    name={"showAllSexes"}
                                    style ={{
                                        color: PALETTE.COLOR_2
                                    }}
                                    checked={allSexesVisible} 
                                    onChange={this._onAllSexesCheckBoxChange} 
                                />
                            }
                            label={
                                <Typography variant="body2">
                                    <Translate id={"statistics.showAllSexes"} />
                                </Typography>     
                            }
                        />
                    </Grid>
                </Grid>
            </Box>
        )
    }
      
    render() {
        const { statisticsBySex, className } = this.props;

        if(!statisticsBySex.loaded) {
            return (
                <Paper className={className}>
                    <Translate id={"loading"} />
                </Paper>
            );
        }

        const { opacity, visibleMeasures, allSexesVisible } = this.state;
        const { numberOfPositiveCases, numberOfDeaths } = statisticsBySex.data;

        const data = numberOfPositiveCases
            .map(item => {
                const {measureId, labelTranslationId, value} = item;
                const label = this.props.translate(labelTranslationId, this.props.currentLanguage);
                const isTotal = measureId === ALL_SEXES_MEASURE_ID;
                const numberOfDeathsItem = numberOfDeaths.find(x => x.measureId === measureId);

                return {
                    measureId,
                    isTotal,
                    label,
                    [POSITIVE_CASES_MEASURE_ID]: value,
                    [DEATHS_MEASURE_ID]: numberOfDeathsItem.value,
                    [MEASURE_ACTUAL_VALUES_MAP[POSITIVE_CASES_MEASURE_ID]]: formatNumber(value, this.props.currentLanguage),
                    [MEASURE_ACTUAL_VALUES_MAP[DEATHS_MEASURE_ID]]: formatNumber(numberOfDeathsItem.value, this.props.currentLanguage)
                }
            })
            .filter(item => allSexesVisible ? true : !item.isTotal);

        return (
            <Paper className={className}>
                <ChartTitle>
                    <Translate id="statistics.statisticsBySexTitle"/>
                    <Box pt={1}>
                        <Typography align="center" color="textSecondary" component="p">
                            <Translate id="statistics.statisticsBySexAbout" />
                        </Typography>
                    </Box>
                </ChartTitle>
                {this._renderFilters()}
                <div style={{ width: '70%', height: '300px', margin: '0 auto'}}>
                    <ResponsiveContainer>
                        <BarChart
                            data={data}
                            margin={{
                                top: 20, right: 30, bottom: 20, left: 20,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="label"/>
                            <YAxis tickFormatter={this._formatYAxis}/>
                            <Tooltip content={
                                <CustomTooltip 
                                    label={"label"}
                                    translate={this.props.translate} 
                                    currentLanguage={this.props.currentLanguage}
                                    measureIds={[POSITIVE_CASES_MEASURE_ID, DEATHS_MEASURE_ID]}
                                    translationsMap={MEASURE_LABEL_TRANSLATION_KEY_MAP}
                                />}
                                cursor={{ fill: PALETTE.COLOR_1, fillOpacity: 0.3 }} 
                            />
                            <Legend 
                                formatter={this._formatLegendItem}
                                onClick={this._onLegendClick}
                                onMouseOver={this._onLegendMouseEnter}
                                onMouseOut={this._onLegendMouseLeave}
                            />
                            <Bar 
                                dataKey={POSITIVE_CASES_MEASURE_ID} 
                                stroke={MEASURE_COLOR_MAP[POSITIVE_CASES_MEASURE_ID]}
                                fill={MEASURE_COLOR_MAP[POSITIVE_CASES_MEASURE_ID]}
                                fillOpacity={opacity[POSITIVE_CASES_MEASURE_ID]}
                                hide={!visibleMeasures.includes(POSITIVE_CASES_MEASURE_ID)}
                            >
                                <LabelList dataKey={MEASURE_ACTUAL_VALUES_MAP[POSITIVE_CASES_MEASURE_ID]} position="top" />
                            </Bar>
                            <Bar 
                                dataKey={DEATHS_MEASURE_ID} 
                                stroke={MEASURE_COLOR_MAP[DEATHS_MEASURE_ID]}
                                fill={MEASURE_COLOR_MAP[DEATHS_MEASURE_ID]}
                                fillOpacity={opacity[DEATHS_MEASURE_ID]}
                                hide={!visibleMeasures.includes(DEATHS_MEASURE_ID)}
                            >
                                <LabelList dataKey={MEASURE_ACTUAL_VALUES_MAP[DEATHS_MEASURE_ID]} position="top" />
                            </Bar>
                        </BarChart>
                    </ResponsiveContainer>
                </div>
            </Paper>
          );
    }
}

StatisticsBySexChart.propTypes = {
    translate: PropTypes.func,
    currentLanguage: PropTypes.string,
    className: PropTypes.string,
    statisticsBySex: PropTypes.object.isRequired,
    statisticsActions: PropTypes.object
};

export default connect(Container.mapStateToProps, Container.mapDispatchToProps)(StatisticsBySexChart);
