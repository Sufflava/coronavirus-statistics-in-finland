import React from "react";
import { withLocalize, Translate } from "react-localize-redux";
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputBase from '@material-ui/core/InputBase';
import { withStyles } from '@material-ui/core/styles';

const BootstrapInput = withStyles((theme) => ({
  root: {
    'label + &': {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 4,
    position: 'relative',
    backgroundColor: theme.palette.background.paper,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 26px 10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderRadius: 4,
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
}))(InputBase);

const useStyles = theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 30,
}});

class LanguageToggle extends React.Component {
    _onLanguageChange = (event) => { 
        const locale = event.target.value;
        this.props.setActiveLanguage(locale);
    }

    render() {
        if(!this.props.languages.length) {
            return null;
        }

        const { classes } = this.props;

        return (
            <FormControl variant="outlined" className={classes.formControl}>
                <Select
                    native
                    value={this.props.activeLanguage.code}
                    onChange={this._onLanguageChange}
                    input={<BootstrapInput />}
                >
                    {this.props.languages.map(lang => (
                        <option key={lang.code} value={lang.code}>
                            {lang.name}
                        </option>
                    ))}
                </Select>  
            </FormControl>
        );
    }
}

export default withStyles(useStyles)(withLocalize(LanguageToggle));
