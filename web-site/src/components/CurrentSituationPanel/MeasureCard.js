import React from 'react';
import PropTypes from 'prop-types';
import { Translate } from "react-localize-redux";
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Tooltip from '@material-ui/core/Tooltip';
import InfoIcon from '@material-ui/icons/Info';
import { formatNumber } from '../../utils';

class MeasureCard extends React.Component {
    render() {
        const { labelTranslationId, value, valueColor, tooltipTranslationId } = this.props;
    
        return (
            <Grid item xs={6}>
                <Paper>
                    <Box p={1}>
                        <Typography color="textPrimary" gutterBottom>
                            <Translate id={labelTranslationId} />
                            &nbsp;
                            {
                                tooltipTranslationId ? 
                                    <Tooltip title={<Translate id={tooltipTranslationId} />} placement="top" arrow enterTouchDelay={500}>
                                        <InfoIcon style={{ fontSize: 20, opacity: 0.3, verticalAlign: "text-top" }}/>
                                    </Tooltip> :
                                    null
                            }
                        </Typography>
                        <Typography variant="h5" component="h2" style={{ color: valueColor, fontWeight: 600 }}>
                            {formatNumber(value, this.props.currentLanguage)}
                        </Typography>
                    </Box>
                </Paper>
            </Grid>
        );
    }
}

MeasureCard.propTypes = {
    translate: PropTypes.func,
    currentLanguage: PropTypes.string,
    labelTranslationId: PropTypes.string,
    value: PropTypes.number,
    valueColor: PropTypes.string,
    tooltipTranslationId: PropTypes.string
};

export default MeasureCard;