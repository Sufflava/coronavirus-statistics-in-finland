import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Translate } from "react-localize-redux";
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import {CurrentSituationContainer as Container} from '../../containers';
import ChartTitle from '../ChartTitle';
import MeasureCard from './MeasureCard';
import { formatDate } from '../../utils';

class CurrentSituationPanel extends React.Component {
    componentDidMount() {
        if(!this.props.countryTotals.loaded) {
            this.props.statisticsActions.fetchCountryTotals();
        }
    }

    _formatDate = (date) => { 
        return formatDate(date, this.props.currentLanguage, 'DD MMMM YYYY');
    }
      
    render() {
        const { countryTotals, className } = this.props;

        if(!countryTotals.loaded) {
            return (
                <Paper className={className}>
                    <Translate id={"loading"} />
                </Paper>
            );
        }

        const { 
            day, population, allReasonsDeaths, numberOfTests, numberOfPositiveCases, 
            numberOfRecovered, numberOfDeaths 
        } = countryTotals.data;

        return (
            <Paper className={className}>
                <ChartTitle>
                    <Translate id="statistics.currentSituationTitle" data={{ date: this._formatDate(day) }}/>
                </ChartTitle>
                <Box pt={0} pr={2} pb={2} pl={2}>
                    <Grid container spacing={2} direction="row" justify="flex-start" alignItems="flex-start">
                        <MeasureCard key="1" labelTranslationId="statistics.population" value={population} valueColor={"#0277BD"}/>
                        <MeasureCard key="2" labelTranslationId="statistics.totalNumberOfTests" value={numberOfTests} valueColor={"#0277BD"}/>
                        <MeasureCard key="3" labelTranslationId="statistics.totalNumberOfPositiveCases" value={numberOfPositiveCases} valueColor={"#FF8F00"}/>
                        <MeasureCard key="4" labelTranslationId="statistics.totalNumberOfRecovered" value={numberOfRecovered} valueColor={"#2E7D32"} tooltipTranslationId="statistics.totalNumberOfRecoveredHint"/>
                        <MeasureCard key="5" labelTranslationId="statistics.totalNumberOfDeaths" value={numberOfDeaths} valueColor={"#D50000"}/>
                        <MeasureCard key="6" labelTranslationId="statistics.totalNumberOfDeathsAllReasonsIn2020" value={allReasonsDeaths["2020"]} valueColor={"#D50000"} tooltipTranslationId="statistics.totalNumberOfDeathsAllReasonsIn2020Hint"/>
                    </Grid>
                </Box>
            </Paper>
          );
    }
}

CurrentSituationPanel.propTypes = {
    translate: PropTypes.func,
    currentLanguage: PropTypes.string,
    className: PropTypes.string,
    countryTotals: PropTypes.object.isRequired,
    statisticsActions: PropTypes.object
};

export default connect(Container.mapStateToProps, Container.mapDispatchToProps)(CurrentSituationPanel);
