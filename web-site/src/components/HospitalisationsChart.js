import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Translate } from "react-localize-redux";
import Paper from '@material-ui/core/Paper'
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import {
    BarChart, ResponsiveContainer, XAxis, YAxis, CartesianGrid, 
    Tooltip, Legend, Bar, LabelList
} from 'recharts';
import {HospitalisationsContainer as Container} from '../containers';
import ChartTitle from './ChartTitle';
import { formatDate, formatNumber } from '../utils';
import {
    HOSPITALISATION_AREA_SHORT_LABEL_TRANSLATION_KEY_MAP, 
    HOSPITALISATION_AREA_LABEL_TRANSLATION_KEY_MAP, PALETTE
} from '../constants';
import { default as CustomTooltip } from './BarChartTooltip';

const DEFAULT_BAR_OPACITY = 0.8;
const IN_SPECIALISED_HEALTH_CARE_MEASURE_ID = "inSpecialisedHealthCare";
const IN_HEALTHCARE_CENTER_HOSPITALS_MEASURE_ID = "inHealthcareCenterHospitals";
const IN_INTENSIVE_CARE_MEASURE_ID = "inIntensiveCare";

const MEASURE_IDS = [
    IN_SPECIALISED_HEALTH_CARE_MEASURE_ID, 
    IN_HEALTHCARE_CENTER_HOSPITALS_MEASURE_ID, 
    IN_INTENSIVE_CARE_MEASURE_ID
];

const MEASURE_LABEL_TRANSLATION_KEY_MAP = {
    [IN_SPECIALISED_HEALTH_CARE_MEASURE_ID]: 'statistics.inSpecialisedHealthCare',
    [IN_HEALTHCARE_CENTER_HOSPITALS_MEASURE_ID]: 'statistics.inHealthcareCenterHospitals',
    [IN_INTENSIVE_CARE_MEASURE_ID]: 'statistics.inIntensiveCare'
};

const MEASURE_COLOR_MAP = {
    [IN_SPECIALISED_HEALTH_CARE_MEASURE_ID]: PALETTE.COLOR_2,
    [IN_HEALTHCARE_CENTER_HOSPITALS_MEASURE_ID]: PALETTE.COLOR_3,
    [IN_INTENSIVE_CARE_MEASURE_ID]: PALETTE.COLOR_4
}; 

class HospitalisationsChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            opacity: {
                [IN_SPECIALISED_HEALTH_CARE_MEASURE_ID]: DEFAULT_BAR_OPACITY,
                [IN_HEALTHCARE_CENTER_HOSPITALS_MEASURE_ID]: DEFAULT_BAR_OPACITY,
                [IN_INTENSIVE_CARE_MEASURE_ID]: DEFAULT_BAR_OPACITY
            },
            visibleMeasures: [
                IN_SPECIALISED_HEALTH_CARE_MEASURE_ID, 
                IN_HEALTHCARE_CENTER_HOSPITALS_MEASURE_ID,
                IN_INTENSIVE_CARE_MEASURE_ID
            ]
        };
    }

    componentDidMount() {
        if(!this.props.hospitalisations.loaded) {
            this.props.statisticsActions.fetchHospitalisations();
        }
    }

    _formatDate = (date) => { 
        return formatDate(date, this.props.currentLanguage, 'DD MMMM YYYY');
    }

    _formatYAxis = (tickItem) => { 
        return formatNumber(tickItem, this.props.currentLanguage);
    }

    _formatLegendItem = (value, entry) => {
        const translationKey = `statistics.${value}`;
        return <Translate id={translationKey} />;
    }

    _toggleMeasute = (e) => {
        const measureId = e.dataKey;
        const visibleMeasures = this.state.visibleMeasures;

        this.setState({
            visibleMeasures: visibleMeasures.includes(measureId) ?
                visibleMeasures.filter(x => x !== measureId) :
                [...visibleMeasures, measureId]
        });
    }

    _onLegendMouseEnter = (o) => {
        const { dataKey } = o;
        const { opacity } = this.state;
    
  	    this.setState({
    	    opacity: { ...opacity, [dataKey]: 1 }
        });
    }

    _onLegendMouseLeave = (o) => {
        const { dataKey } = o;
        const { opacity } = this.state;
    
  	    this.setState({
    	    opacity: { 
                ...opacity, 
                [dataKey]: DEFAULT_BAR_OPACITY 
            },
        });
    }
      
    render() {
        const { hospitalisations, className } = this.props;

        if(!hospitalisations.loaded) {
            return (
                <Paper className={className}>
                    <Translate id={"loading"} />
                </Paper>
            );
        }

        const { opacity, visibleMeasures } = this.state;
        const { hospitalisationsByArea } = hospitalisations.data;
        const day = hospitalisationsByArea.day;

        const data = hospitalisationsByArea.values
            .map(item => {
                const {areaId} = item;
                const areaShortLabel = this.props.translate(HOSPITALISATION_AREA_SHORT_LABEL_TRANSLATION_KEY_MAP[areaId], this.props.currentLanguage);
                const areaFullLabel = this.props.translate(HOSPITALISATION_AREA_LABEL_TRANSLATION_KEY_MAP[areaId], this.props.currentLanguage);
                
                return {
                    ...item,
                    areaShortLabel,
                    areaFullLabel
                }
            });

        return (
            <Paper className={className}>
                <ChartTitle>
                    <Translate id="statistics.numberOfPeopleInHospitalsTitle" data={{ date: this._formatDate(day) }}/>
                    <Box pt={1}>
                        <Typography align="center" color="textSecondary" component="p">
                            <Translate id="statistics.hospitalisationsAbout" 
                                data={{ 
                                    thlLink: <Link href="https://thl.fi/en/web/infectious-diseases-and-vaccinations/what-s-new/coronavirus-covid-19-latest-updates/situation-update-on-coronavirus" target="_blank" rel="noreferrer"><Translate id="layout.here" /></Link>,
                                }} 
                            />
                        </Typography>
                    </Box>
                </ChartTitle>
                <div style={{ width: '100%', height: '300px'}}>
                    <ResponsiveContainer>
                        <BarChart
                            data={data}
                            margin={{
                                top: 20, right: 30, bottom: 20, left: 20,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="areaShortLabel"/>
                            <YAxis tickFormatter={this._formatYAxis}/>
                            <Tooltip content={
                                <CustomTooltip 
                                    labelPropName={"areaFullLabel"}
                                    translate={this.props.translate} 
                                    currentLanguage={this.props.currentLanguage}
                                    measureIds={MEASURE_IDS}
                                    translationsMap={MEASURE_LABEL_TRANSLATION_KEY_MAP}
                                />}
                                cursor={{ fill: PALETTE.COLOR_1, fillOpacity: 0.3 }} 
                            />
                            <Legend 
                                formatter={this._formatLegendItem}
                                onClick={this._toggleMeasute}
                                onMouseOver={this._onLegendMouseEnter}
                                onMouseOut={this._onLegendMouseLeave}
                            />
                            {
                                MEASURE_IDS.map(measureId => (
                                    <Bar
                                        key={measureId} 
                                        dataKey={measureId} 
                                        stroke={MEASURE_COLOR_MAP[measureId]}
                                        fill={MEASURE_COLOR_MAP[measureId]}
                                        fillOpacity={opacity[measureId]}
                                        hide={!visibleMeasures.includes(measureId)}
                                    >
                                        <LabelList dataKey={measureId} position="top" />
                                    </Bar>
                                ))
                            }
                        </BarChart>
                    </ResponsiveContainer>
                </div>
            </Paper>
          );
    }
}

HospitalisationsChart.propTypes = {
    translate: PropTypes.func,
    currentLanguage: PropTypes.string,
    className: PropTypes.string,
    hospitalisations: PropTypes.object.isRequired,
    statisticsActions: PropTypes.object
};

export default connect(Container.mapStateToProps, Container.mapDispatchToProps)(HospitalisationsChart);
