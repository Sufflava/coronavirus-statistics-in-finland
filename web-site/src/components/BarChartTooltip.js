import React from 'react';
import PropTypes from 'prop-types';
import { Translate } from "react-localize-redux";

class BarChartTooltip extends React.Component {
    _getFormattedNumber = (value) => { 
        return value ? value.toLocaleString(this.props.currentLanguage) : value;
    }

    _renderMeasureLabel = (measureId) => {
        const {translationsMap} = this.props;
        const translationKey = translationsMap[measureId];
        return <Translate id={translationKey} />;
    }
  
    render() {
        const { active, payload, label, labelPropName, separator, measureIds, actualValuesKeys } = this.props;

        if(!active || !payload.length) {
            return null;
        }

        const point = payload[0].payload;
        const colors = {};

        payload.forEach(x => {
            colors[x.dataKey] = x.stroke;
        });

        return (
            <div className="custom-tooltip">
                <div className="title">
                    { labelPropName ? point[labelPropName] : label }
                </div>
                {
                    measureIds.map(id => (
                        <div key={id} className="measure" style={colors[id] ? {color: colors[id] } : null}>
                            {this._renderMeasureLabel(id)}
                            {separator}
                            {
                                actualValuesKeys ? 
                                    point[actualValuesKeys[id]] :
                                    this._getFormattedNumber(point[id])
                            }
                        </div>
                    ))
                }
            </div>
        );
    }
}

BarChartTooltip.propTypes = {
    translate: PropTypes.func,
    currentLanguage: PropTypes.string,
    type: PropTypes.string,
    payload: PropTypes.array,
    label: PropTypes.string,
    labelPropName: PropTypes.string,
    separator: PropTypes.string,
    measureIds: PropTypes.array,
    translationsMap: PropTypes.object,
    actualValuesKeys: PropTypes.object
};

export default BarChartTooltip;