import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    ComposedChart, Line, Area, ResponsiveContainer, XAxis, YAxis, CartesianGrid, 
    Tooltip, Legend
} from 'recharts';
import { Translate } from "react-localize-redux";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import { formatNumber, getWeekInfo } from '../../utils';
import { RegionalStatisticsContainer as Container } from '../../containers';
import {
    NUMBER_OF_TESTS_MEASURE_ID, NUMBER_OF_POSITIVE_CASES_MEASURE_ID, 
    ALL_REGIONS_MEASURE_ID, ALL_WEEKS_MEASURE_ID, HUS_REGION_MEASURE_ID, PALETTE,
    MEASURE_LABEL_TRANSLATION_KEY_MAP
} from '../../constants';
import CustomTooltip from './WeeklyRegionalStatisticsTooltip';
import Paper from '@material-ui/core/Paper';
import ChartTitle from '../ChartTitle';

const DEFAULT_LINE_OPACITY = 0.8;

const MEASURE_IDS = [NUMBER_OF_TESTS_MEASURE_ID, NUMBER_OF_POSITIVE_CASES_MEASURE_ID];

const MEASURE_COLOR_MAP = {
    [NUMBER_OF_TESTS_MEASURE_ID]: PALETTE.COLOR_3,
    [NUMBER_OF_POSITIVE_CASES_MEASURE_ID]: PALETTE.COLOR_4
};

function CustomizedAxisTick(props) {
    const {x, y, stroke, payload} = props;
    const weekInfo = getWeekInfo(payload.value);

    return (
        <g transform={`translate(${x},${y})`}>
            <text x={0} y={0} dy={16} textAnchor="end" fill="#666666" transform="rotate(-35)">
                <Translate id={"statistics.week"} 
                    data={{ 
                        week: weekInfo.week,
                        year: weekInfo.year
                    }} 
                />
            </text>
        </g>
    );
}
  
class WeeklyRegionalStatisticsChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            opacity: {
                [NUMBER_OF_TESTS_MEASURE_ID]: DEFAULT_LINE_OPACITY,
                [NUMBER_OF_POSITIVE_CASES_MEASURE_ID]: DEFAULT_LINE_OPACITY
            },
            visibleMeasures: [
                NUMBER_OF_TESTS_MEASURE_ID, 
                NUMBER_OF_POSITIVE_CASES_MEASURE_ID
            ],
            selectedRegionMeasureId: HUS_REGION_MEASURE_ID
        };
    }

    componentDidMount() {
        if(!this.props.weeklyRegionalStatistics.loaded) {
            this.props.statisticsActions.fetchWeeklyRegionalStatistics();
        }
    }

    _formatXAxis = (tickItem) => { 
        return formatNumber(tickItem, this.props.currentLanguage);
    }

    _formatYAxis = (tickItem) => { 
        return formatNumber(tickItem, this.props.currentLanguage);
    }

    _formatLegendItem = (value, entry) => {
        const translationKey = MEASURE_LABEL_TRANSLATION_KEY_MAP[value];
        return <Translate id={translationKey} />;
    }

    _onLegendClick = (e) => {
        const measureId = e.dataKey;
        this._toggleMeasure(measureId);
    }

    _onMeasureCheckBoxChange = (event) => {
        const measureId = event.target.name;
        this._toggleMeasure(measureId);      
    }

    _toggleMeasure = (measureId) => {
        const visibleMeasures = this.state.visibleMeasures;

        this.setState({
            visibleMeasures: visibleMeasures.includes(measureId) ?
                visibleMeasures.filter(x => x !== measureId) :
                [...visibleMeasures, measureId]
        });
    }

    _onLegendMouseEnter = (o) => {
        const { dataKey } = o;
        const { opacity } = this.state;
    
  	    this.setState({
    	    opacity: { ...opacity, [dataKey]: 1 }
        });
    }

    _onLegendMouseLeave = (o) => {
        const { dataKey } = o;
        const { opacity } = this.state;
    
  	    this.setState({
    	    opacity: { 
                ...opacity, 
                [dataKey]: DEFAULT_LINE_OPACITY 
            },
        });
    }

    _onRegionChange = (event) => {
        this.setState({
            selectedRegionMeasureId: event.target.value
        });
    }

    _renderFilters = () => {
        const { visibleMeasures, selectedRegionMeasureId } = this.state;
        const regions = this.props.weeklyRegionalStatistics.data.regions;
        const regionOptions = Object.keys(regions)
            .map(key => ({ id: key, name: regions[key]}))
            .filter(x => x.id !== ALL_REGIONS_MEASURE_ID)
            .sort((a, b) => {
                return a.name.localeCompare(b.name);
            });

        return (
            <Box pl={4} pr={2} pb={2}>
                <Grid container direction="row" justify="center" alignItems="flex-end">
                    <Grid item xs={12} sm={6}>
                        <FormControl>
                            <InputLabel shrink id="region-label">
                                <Translate id={"statistics.hcdRegion"} />
                            </InputLabel>
                            <Select
                                labelId="region-label"
                                value={selectedRegionMeasureId}
                                onChange={this._onRegionChange}
                            >
                                {
                                    regionOptions.map(item => (
                                        <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>
                                    ))
                                }
                            </Select>
                        </FormControl>
                    </Grid>
                    {MEASURE_IDS.map(id => (
                        <Grid key={id} item xs={6} sm={3}>
                            <FormControlLabel
                                control={
                                    <Checkbox 
                                        name={id}
                                        checked={visibleMeasures.includes(id)} 
                                        onChange={this._onMeasureCheckBoxChange} 
                                        style ={{
                                            color: MEASURE_COLOR_MAP[id],
                                        }}
                                    />
                                }
                                label={
                                    <Typography variant="body2" style={{ color: MEASURE_COLOR_MAP[id] }}>
                                        <Translate id={MEASURE_LABEL_TRANSLATION_KEY_MAP[id]} />
                                    </Typography>     
                                }
                            />
                        </Grid>
                    ))}
                </Grid>
            </Box>
        )
    }
      
    render() {
        const { className, weeklyRegionalStatistics } = this.props;

        if(!weeklyRegionalStatistics.loaded) {
            return (
                <Paper className={className}>
                    <Translate id={"loading"} />
                </Paper>
            );
        }

        const { opacity, visibleMeasures, selectedRegionMeasureId } = this.state;

        const data = weeklyRegionalStatistics.data.items
            .filter(item => item.regionMeasureId === selectedRegionMeasureId && 
                item.weekId !== ALL_WEEKS_MEASURE_ID);

        return (
            <Paper className={className}>
                <ChartTitle>
                    <Translate id="statistics.weeklyRegionalStatisticsChartTitle" />
                    <Box pt={1}>
                        <Typography align="center" color="textSecondary" component="p">
                            <Translate id="statistics.weeklyRegionalStatisticsAbout" />
                        </Typography>
                    </Box>
                </ChartTitle>
                {this._renderFilters()}
                <div style={{ width: '100%', height: '600px'}}>
                    <ResponsiveContainer>
                        <ComposedChart
                            data={data}
                            margin={{
                                top: 20, right: 30, bottom: 20, left: 20,
                            }}
                        >
                            <CartesianGrid stroke="#f5f5f5" />
                            <XAxis 
                                dataKey="weekLabel" 
                                height={110} 
                                tick={
                                    <CustomizedAxisTick currentLanguage={this.props.currentLanguage}/>
                                }
                            />
                            <YAxis tickFormatter={this._formatYAxis}/>
                            <Tooltip content={
                                <CustomTooltip 
                                    translate={this.props.translate} 
                                    currentLanguage={this.props.currentLanguage}
                                    measureIds={MEASURE_IDS}
                                />}
                            />
                            <Legend 
                                formatter={this._formatLegendItem} 
                                onClick={this._onLegendClick}
                                onMouseOver={this._onLegendMouseEnter}
                                onMouseOut={this._onLegendMouseLeave}
                            />
                            <Line 
                                type="monotone" 
                                strokeOpacity={opacity[NUMBER_OF_TESTS_MEASURE_ID]} 
                                dataKey={NUMBER_OF_TESTS_MEASURE_ID} 
                                stroke={MEASURE_COLOR_MAP[NUMBER_OF_TESTS_MEASURE_ID]}
                                hide={!visibleMeasures.includes(NUMBER_OF_TESTS_MEASURE_ID)}
                                dot={false}
                            />
                            <Area 
                                type="monotone" 
                                dataKey={NUMBER_OF_POSITIVE_CASES_MEASURE_ID} 
                                hide={!visibleMeasures.includes(NUMBER_OF_POSITIVE_CASES_MEASURE_ID)}
                                fill={MEASURE_COLOR_MAP[NUMBER_OF_POSITIVE_CASES_MEASURE_ID]}
                                stroke={MEASURE_COLOR_MAP[NUMBER_OF_POSITIVE_CASES_MEASURE_ID]}
                                fillOpacity={opacity[NUMBER_OF_POSITIVE_CASES_MEASURE_ID]}
                            />
                        </ComposedChart>
                    </ResponsiveContainer>
                </div>
            </Paper>
          );
    }
}

WeeklyRegionalStatisticsChart.propTypes = {
    translate: PropTypes.func,
    currentLanguage: PropTypes.string,
    className: PropTypes.string,
    weeklyRegionalStatistics: PropTypes.object.isRequired,
    statisticsActions: PropTypes.object
};

export default connect(Container.mapStateToProps, Container.mapDispatchToProps)(WeeklyRegionalStatisticsChart);
