import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    ComposedChart, Area, ResponsiveContainer, XAxis, YAxis, CartesianGrid, 
    Tooltip, Legend
} from 'recharts';
import { Translate } from "react-localize-redux";
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import { formatDate, formatNumber } from '../../utils';
import { RegionalStatisticsContainer as Container } from '../../containers';
import { 
    NUMBER_OF_POSITIVE_CASES_MEASURE_ID, MEASURE_LABEL_TRANSLATION_KEY_MAP,
    ALL_REGIONS_MEASURE_ID, HUS_REGION_MEASURE_ID, PALETTE 
} from '../../constants';
import CustomTooltip from './DailyRegionalStatisticsTooltip';
import Paper from '@material-ui/core/Paper';
import ChartTitle from '../ChartTitle';

const MEASURE_IDS = [NUMBER_OF_POSITIVE_CASES_MEASURE_ID];

const MEASURE_COLOR_MAP = {  
    [NUMBER_OF_POSITIVE_CASES_MEASURE_ID]: PALETTE.COLOR_4,
};

function CustomizedAxisTick(props) {
    const {x, y, stroke, payload} = props;
          
    return (
        <g transform={`translate(${x},${y})`}>
            <text x={0} y={0} dy={16} textAnchor="end" fill="#666666" transform="rotate(-35)">
                {formatDate(payload.value, props.currentLanguage, 'DD MMM')}
            </text>
        </g>
    );
}
  
class DailyRegionalStatisticsChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            selectedRegionMeasureId: HUS_REGION_MEASURE_ID
        };
    }

    componentDidMount() {
        if(!this.props.dailyRegionalStatistics.loaded) {
            this.props.statisticsActions.fetchDailyRegionalStatistics();
        }
    }

    _formatXAxis = (tickItem) => { 
        return formatDate(tickItem, this.props.currentLanguage, 'DD MMM');
    }

    _formatYAxis = (tickItem) => { 
        return formatNumber(tickItem, this.props.currentLanguage);
    }

    _formatLegendItem = (value, entry) => {
        const translationKey = MEASURE_LABEL_TRANSLATION_KEY_MAP[value];
        return <Translate id={translationKey} />;
    }

    _onRegionChange = (event) => {
        this.setState({
            selectedRegionMeasureId: event.target.value
        });
    }

    _renderFilters = () => {
        const { selectedRegionMeasureId } = this.state;
        
        const regions = this.props.dailyRegionalStatistics.data.regions;
        
        const regionOptions = Object.keys(regions)
            .map(key => ({ id: key, name: regions[key]}))
            .filter(x => x.id !== ALL_REGIONS_MEASURE_ID)
            .sort((a, b) => {
                return a.name.localeCompare(b.name);
            });

        return (
            <Box pl={8} pr={2} pb={2}>
                <FormControl>
                    <InputLabel shrink id="region-label">
                        <Translate id={"statistics.hcdRegion"} />
                    </InputLabel>
                    <Select
                        labelId="region-label"
                        value={selectedRegionMeasureId}
                        onChange={this._onRegionChange}
                    >
                        {
                            regionOptions.map(item => (
                                <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>
                            ))
                        }
                    </Select>
                </FormControl>
            </Box>
        );
    }
      
    render() {
        const { className, dailyRegionalStatistics } = this.props;

        if(!dailyRegionalStatistics.loaded) {
            return (
                <Paper className={className}>
                    <Translate id={"loading"} />
                </Paper>
            );
        }

        const { selectedRegionMeasureId } = this.state;

        const data = dailyRegionalStatistics.data.items
            .filter(item => item.regionMeasureId === selectedRegionMeasureId);

        return (
            <Paper className={className}>
                <ChartTitle>
                    <Translate id="statistics.dailyRegionalStatisticsChartTitle" />
                    <Box pt={1}>
                        <Typography align="center" color="textSecondary" component="p">
                            <Translate id="statistics.dailyRegionalStatisticsAbout" />
                        </Typography>
                    </Box>
                </ChartTitle>
                {this._renderFilters()}
                <div style={{ width: '100%', height: '350px'}}>
                    <ResponsiveContainer>
                        <ComposedChart
                            data={data}
                            margin={{
                                top: 20, right: 30, bottom: 20, left: 20,
                            }}
                        >
                            <CartesianGrid stroke="#f5f5f5" />
                            <XAxis 
                                dataKey="date" 
                                height={60} 
                                tick={
                                    <CustomizedAxisTick currentLanguage={this.props.currentLanguage}/>
                                }
                            />
                            <YAxis tickFormatter={this._formatYAxis}/>
                            <Tooltip content={
                                <CustomTooltip 
                                    translate={this.props.translate} 
                                    currentLanguage={this.props.currentLanguage}
                                    measureIds={MEASURE_IDS}
                                />}
                            />
                            <Legend formatter={this._formatLegendItem} />
                            <Area 
                                type="monotone" 
                                dataKey={NUMBER_OF_POSITIVE_CASES_MEASURE_ID} 
                                fill={MEASURE_COLOR_MAP[NUMBER_OF_POSITIVE_CASES_MEASURE_ID]}
                                stroke={MEASURE_COLOR_MAP[NUMBER_OF_POSITIVE_CASES_MEASURE_ID]}
                                fillOpacity={0.8}
                            />
                        </ComposedChart>
                    </ResponsiveContainer>
                </div>
            </Paper>
          );
    }
}

DailyRegionalStatisticsChart.propTypes = {
    translate: PropTypes.func,
    currentLanguage: PropTypes.string,
    className: PropTypes.string,
    dailyRegionalStatistics: PropTypes.object.isRequired,
    statisticsActions: PropTypes.object,
    filtersActions: PropTypes.object
};

export default connect(Container.mapStateToProps, Container.mapDispatchToProps)(DailyRegionalStatisticsChart);
