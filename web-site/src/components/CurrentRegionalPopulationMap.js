import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Translate } from "react-localize-redux";
import * as d3 from 'd3';
import * as turf from '@turf/turf';
import Paper from '@material-ui/core/Paper'
import Box from '@material-ui/core/Box';
import { RegionalStatisticsContainer as Container } from '../containers';
import ChartTitle from './ChartTitle';
import { formatDate, formatNumber } from '../utils';
import { PALETTE } from '../constants';
import {hcdRegionsMapGeoJson} from '../data';

class CurrentRegionalPopulationMap extends React.Component {
    constructor(props){
        super(props);
        this.mapRef = React.createRef(); 

        this.state = { 
            mapDrawn: false
        };
    }

    componentDidUpdate(prevProps) {
        if(!this.props.regionalTotals.loaded) {
            this.props.statisticsActions.fetchRegionalTotals();
        }

        if(this.props.regionalTotals.loaded && !this.state.mapDrawn && this.mapRef.current) {
            this.setState({mapDrawn: true});
            this._drawMap();
        }

        if (this.state.mapDrawn && this.props.currentLanguage !== prevProps.currentLanguage) {
            this.setState({mapDrawn: false});
        }
    }

    _drawMap() {
        d3.select(this.mapRef.current)
            .select("svg")
            .remove();

        const container = d3.select(this.mapRef.current)
            .node()
            .getBoundingClientRect(); 
  
        const width = container.width;
        const height = container.height;

        // https://bost.ocks.org/mike/map/ - instruction for drawing a map using d3 and geojson
        // https://stackoverflow.com/questions/45880033/mercator-causing-blank-d3-page
        const projection = d3.geoMercator()
            .scale(900) // make the map X times bigger (the right scale should be found by hands)
            .translate([width*(-0.5) - 100, height*3.5 + 20]); // position the map inside the container (the right position should be found by hands)

        const path = d3.geoPath(projection);

        const svg = d3.select(this.mapRef.current)
            .append("svg")
                .attr("width", width)
                .attr("height", height)
            .append("g"); 
            
        const tooltip = d3.select(this.mapRef.current)
            .append("div")
                .classed("map-tooltip", true)
                .text("");

        const tooltipHeader = tooltip
            .append("div")
                .classed("map-tooltip-header", true);

        const tooltipValue = tooltip.append("div");

        const populationByRegion = {};

        this.props.regionalTotals.data.population.forEach(data => {
            populationByRegion[data.hcdRegionId] = {
                name: data.regionName,
                population: data.value
            };
        });

        const minStatValue = 30000; 
        const maxStatValue = 1800000; 
        
        const colorScale = d3.scaleLinear()
            .domain([minStatValue, maxStatValue])
            .range([PALETTE.COLOR_2, PALETTE.COLOR_3]);

        // https://stackoverflow.com/questions/49311001/d3-js-osm-geojson-black-rectangle
        // http://turfjs.org/docs/#rewind
        const features = hcdRegionsMapGeoJson.features
            .filter(feature => feature.properties.koodi !== "hcd1") // filter out "All" area
            .map(feature => {
                const regionId = feature.properties.koodi;
                const data = populationByRegion[regionId];
                return ({
                    ...feature,
                    properties: {
                        id: feature.properties.koodi,
                        nameOld: feature.properties.sairaanhoitopiiri,
                        name: data.name,
                        population: data.population,
                        populationFormatted: this._formatNumber(data.population)
                    }
                });
            })
            .map(function(feature) {
                return turf.rewind(feature,{reverse:true});
            });

        svg.selectAll("path")
            .data(features)
            .enter()
            .append("path")
                .attr("d", path)
                .attr("hcdId", function(d) { 
                    return d.properties.id;
                })
                .attr("hcdName", function(d) { 
                    return d.properties.name;
                })
                .attr("hcdPopulation", function(d) { 
                    return d.properties.populationFormatted;
                })
                .classed("map-region", true)
                .style("fill", function (d) { 
                    const value = d.properties.population;
                    const color = colorScale(value);
                    return color; 
                })
                .style("stroke", PALETTE.COLOR_3)
                .on("mouseover", function(event) {
                    const id = event.target.attributes.hcdId.value;
                    const name = event.target.attributes.hcdName.value;
                    const population = event.target.attributes.hcdPopulation.value;
                    
                    tooltipHeader.text(name);
                    tooltipValue.text(population);

                    return tooltip.style("visibility", "visible");
                })
                .on("mousemove", function(event) {
                    return tooltip
                        .style("top", (event.pageY - 55) + "px")
                        .style("left", event.pageX + "px")
                        .style("transform", "translateX(-50%)");
                })
                .on("mouseout", function() {
                    return tooltip.style("visibility", "hidden");
                });

        const defs = svg.append("defs");
        const linearGradient = defs.append("linearGradient")
            .attr("id", "linear-gradient");

        // https://www.visualcinnamon.com/2016/05/smooth-color-legend-d3-svg-gradient
        // Vertical gradient
        linearGradient
            .attr("x1", "0%")
            .attr("y1", "0%")
            .attr("x2", "0%")
            .attr("y2", "100%");

        linearGradient.append("stop")
            .attr("offset", "0%")
            .attr("stop-color", PALETTE.COLOR_3);

        linearGradient.append("stop")
            .attr("offset", "100%")
            .attr("stop-color", PALETTE.COLOR_2);

        const legendWrapper = svg.append("g")
            .classed("map-legend-wrapper", true)
            .style("transform", "translate(" + (width - 50) + "px, 20px)");

        const legendHeight = 150;
        const textHeight = 20;

        legendWrapper.append("text")
            .attr("x", -25)
            .attr("y", textHeight/2) 
            .text(this._formatNumber(maxStatValue)); 

        legendWrapper.append("rect")
            .attr("x", 0)
            .attr("y", textHeight) 
            .attr("width", 20)
            .attr("height", legendHeight)
            .style("fill", "url(#linear-gradient)");

        legendWrapper.append("text")
            .attr("x", -15)
            .attr("y", legendHeight + textHeight*2) 
            .text(this._formatNumber(minStatValue)); 
    }

    _formatDate = (date) => { 
        return formatDate(date, this.props.currentLanguage, 'DD MMMM YYYY');
    }

    _formatNumber = (number) => { 
        return formatNumber(number, this.props.currentLanguage);
    }
      
    render() {
        const { regionalTotals, className } = this.props;

        if(!regionalTotals.loaded) {
            return (
                <Paper className={className}>
                    <Translate id={"loading"} />
                </Paper>
            );
        }

        const day = regionalTotals.data.day;

        return (
            <Paper className={className}>
                <ChartTitle>
                    <Translate id="statistics.regionalPopulationTitle" data={{ date: this._formatDate(day) }}/>
                </ChartTitle>
                <Box pb={2} >
                    <div className="map-container" ref={this.mapRef}>
                    </div>
                </Box>
            </Paper>
          );
    }
}

CurrentRegionalPopulationMap.propTypes = {
    translate: PropTypes.func,
    currentLanguage: PropTypes.string,
    className: PropTypes.string,
    regionalTotals: PropTypes.object.isRequired,
    statisticsActions: PropTypes.object
};

export default connect(Container.mapStateToProps, Container.mapDispatchToProps)(CurrentRegionalPopulationMap);
