import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
    ComposedChart, Line, Area, ResponsiveContainer, XAxis, YAxis, CartesianGrid, 
    Tooltip, Legend
} from 'recharts';
import { Translate } from "react-localize-redux";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { formatDate, formatNumber } from '../../utils';
import {AllMeasuresChartContainer as Container} from '../../containers';
import {
    NUMBER_OF_TESTS_MEASURE_ID, NUMBER_OF_POSITIVE_CASES_MEASURE_ID, 
    NUMBER_OF_DEATHS_MEASURE_ID, MEASURE_LABEL_TRANSLATION_KEY_MAP,
    PALETTE
} from '../../constants';
import CustomTooltip from './AllMeasuresTooltip';
import Paper from '@material-ui/core/Paper';
import ChartTitle from '../ChartTitle';

const MEASURE_IDS = [NUMBER_OF_TESTS_MEASURE_ID, NUMBER_OF_POSITIVE_CASES_MEASURE_ID, NUMBER_OF_DEATHS_MEASURE_ID];

const MEASURE_COLOR_MAP = {
    [NUMBER_OF_TESTS_MEASURE_ID]: PALETTE.COLOR_3, // "#ff7c43",  
    [NUMBER_OF_POSITIVE_CASES_MEASURE_ID]: PALETTE.COLOR_4, // "#a05195", 
    [NUMBER_OF_DEATHS_MEASURE_ID]: PALETTE.COLOR_5 // "#2f4b7c" 
};

const DEFAULT_LINE_OPACITY = 0.8;

function CustomizedAxisTick(props) {
    const {x, y, stroke, payload} = props;
          
    return (
        <g transform={`translate(${x},${y})`}>
            <text x={0} y={0} dy={16} textAnchor="end" fill="#666666" transform="rotate(-35)">
                {formatDate(payload.value, props.currentLanguage, 'DD MMM')}
            </text>
        </g>
    );
}
  
class AllMeasuresChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            opacity: {
                [NUMBER_OF_TESTS_MEASURE_ID]: DEFAULT_LINE_OPACITY,
                [NUMBER_OF_POSITIVE_CASES_MEASURE_ID]: DEFAULT_LINE_OPACITY,
                [NUMBER_OF_DEATHS_MEASURE_ID]: DEFAULT_LINE_OPACITY
            },
            visibleMeasures: [
                NUMBER_OF_TESTS_MEASURE_ID, 
                NUMBER_OF_POSITIVE_CASES_MEASURE_ID, 
                NUMBER_OF_DEATHS_MEASURE_ID
            ]
        };
    }

    componentDidMount() {
        if(!this.props.countryStatistics.loaded) {
            this.props.statisticsActions.fetchCountryStatistics();
        }
    }

    _formatXAxis = (tickItem) => { 
        return formatDate(tickItem, this.props.currentLanguage, 'DD MMM');
    }

    _formatYAxis = (tickItem) => { 
        return formatNumber(tickItem, this.props.currentLanguage);
    }

    _formatLegendItem = (value, entry) => {
        const translationKey = MEASURE_LABEL_TRANSLATION_KEY_MAP[value];
        return <Translate id={translationKey} />;
    }

    _onLegendClick = (e) => {
        const measureId = e.dataKey;
        this._toggleMeasure(measureId);
    }

    _onMeasureCheckBoxChange = (event) => {
        const measureId = event.target.name;
        this._toggleMeasure(measureId);      
    }

    _toggleMeasure = (measureId) => {
        const visibleMeasures = this.state.visibleMeasures;

        this.setState({
            visibleMeasures: visibleMeasures.includes(measureId) ?
                visibleMeasures.filter(x => x !== measureId) :
                [...visibleMeasures, measureId]
        });
    }

    _onLegendMouseEnter = (o) => {
        const { dataKey } = o;
        const { opacity } = this.state;
    
  	    this.setState({
    	    opacity: { ...opacity, [dataKey]: 1 }
        });
    }

    _onLegendMouseLeave = (o) => {
        const { dataKey } = o;
        const { opacity } = this.state;
    
  	    this.setState({
    	    opacity: { 
                ...opacity, 
                [dataKey]: DEFAULT_LINE_OPACITY 
            },
        });
    }

    _renderFilters = () => {
        const { visibleMeasures } = this.state;

        return (
            <Box pl={2} pr={2}>
                <Grid container direction="row" justify="center" alignItems="baseline">
                    {MEASURE_IDS.map(id => (
                        <Grid key={id} item xs={6} sm={3}>
                            <FormControlLabel
                                control={
                                    <Checkbox 
                                        name={id}
                                        checked={visibleMeasures.includes(id)} 
                                        onChange={this._onMeasureCheckBoxChange} 
                                        style ={{
                                            color: MEASURE_COLOR_MAP[id],
                                        }}
                                    />
                                }
                                label={
                                    <Typography variant="body2" style={{ color: MEASURE_COLOR_MAP[id] }}>
                                        <Translate id={MEASURE_LABEL_TRANSLATION_KEY_MAP[id]} />
                                    </Typography>     
                                }
                            />
                        </Grid>
                    ))}
                </Grid>
            </Box>
        )
    }
      
    render() {
        const { className, countryStatistics } = this.props;
        const { visibleMeasures, opacity } = this.state;

        if(!countryStatistics.loaded) {
            return (
                <Paper className={className}>
                    <Translate id={"loading"} />
                </Paper>
            );
        }

        return (
            <Paper className={className}>
                <ChartTitle>
                    <Translate id="statistics.allMeasuresChartTitle" />
                </ChartTitle>
                {this._renderFilters()}
                <div style={{ width: '100%', height: '550px'}}>
                    <ResponsiveContainer>
                        <ComposedChart
                            data={countryStatistics.items}
                            margin={{
                                top: 20, right: 30, bottom: 20, left: 20,
                            }}
                        >
                            <CartesianGrid stroke="#f5f5f5" />
                            <XAxis 
                                dataKey="date" 
                                height={60} 
                                tick={
                                    <CustomizedAxisTick currentLanguage={this.props.currentLanguage}/>
                                }
                            />
                            <YAxis tickFormatter={this._formatYAxis}/>
                            <Tooltip content={
                                <CustomTooltip 
                                    translate={this.props.translate} 
                                    currentLanguage={this.props.currentLanguage}
                                    measureIds={MEASURE_IDS}
                                />}
                            />
                            <Legend 
                                formatter={this._formatLegendItem} 
                                onClick={this._onLegendClick}
                                onMouseOver={this._onLegendMouseEnter}
                                onMouseOut={this._onLegendMouseLeave}
                            />
                            <Line 
                                type="monotone" 
                                strokeOpacity={opacity[NUMBER_OF_TESTS_MEASURE_ID]} 
                                dataKey={NUMBER_OF_TESTS_MEASURE_ID} 
                                stroke={MEASURE_COLOR_MAP[NUMBER_OF_TESTS_MEASURE_ID]}
                                hide={!visibleMeasures.includes(NUMBER_OF_TESTS_MEASURE_ID)}
                                dot={false}
                            />
                            {
                                [NUMBER_OF_POSITIVE_CASES_MEASURE_ID, NUMBER_OF_DEATHS_MEASURE_ID].map(id => (
                                    <Area 
                                        key={id}
                                        type="monotone" 
                                        dataKey={id} 
                                        hide={!visibleMeasures.includes(id)}
                                        fill={MEASURE_COLOR_MAP[id]}
                                        stroke={MEASURE_COLOR_MAP[id]}
                                        fillOpacity={opacity[id]}
                                    />
                                ))
                            }
                        </ComposedChart>
                    </ResponsiveContainer>
                </div>
            </Paper>
          );
    }
}

AllMeasuresChart.propTypes = {
    translate: PropTypes.func,
    currentLanguage: PropTypes.string,
    className: PropTypes.string,
    countryStatistics: PropTypes.object.isRequired,
    statisticsActions: PropTypes.object,
    filtersActions: PropTypes.object
};

export default connect(Container.mapStateToProps, Container.mapDispatchToProps)(AllMeasuresChart);
