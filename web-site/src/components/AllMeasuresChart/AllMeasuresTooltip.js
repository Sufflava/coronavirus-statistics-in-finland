import React from 'react';
import PropTypes from 'prop-types';
import { Translate } from "react-localize-redux";
import { formatDate } from '../../utils';
import {MEASURE_LABEL_TRANSLATION_KEY_MAP} from '../../constants';

class AllMeasuresTooltip extends React.Component {
    _getFormattedDate = (date) => {
        return formatDate(date, this.props.currentLanguage, 'DD MMMM YYYY');
    }

    _getFormattedNumber = (value) => { 
        if(value === undefined) {
            return "-";
        }

        return value ? value.toLocaleString(this.props.currentLanguage) : value;
    }

    _renderMeasureLabel = (measureId) => {
        const translationKey = MEASURE_LABEL_TRANSLATION_KEY_MAP[measureId];
        return <Translate id={translationKey} />;
    }
  
    render() {
        const { active, payload, label, separator, measureIds } = this.props;

        if(!active || !payload.length) {
            return null;
        }

        const point = payload[0].payload;
        const colors = {};

        payload.forEach(x => {
            colors[x.dataKey] = x.stroke;
        });

        return (
            <div className="custom-tooltip">
                <div className="title">
                    {this._getFormattedDate(label)}
                </div>
                {
                    measureIds.map(id => (
                        <div key={id} className="measure" style={colors[id] ? {color: colors[id] } : null}>
                            {this._renderMeasureLabel(id)}
                            {separator}
                            {this._getFormattedNumber(point[id])}
                        </div>
                    ))
                }
            </div>
        );
    }
}

AllMeasuresTooltip.propTypes = {
    translate: PropTypes.func,
    currentLanguage: PropTypes.string,
    type: PropTypes.string,
    payload: PropTypes.array,
    label: PropTypes.string,
    separator: PropTypes.string,
    measureIds: PropTypes.array
};

export default AllMeasuresTooltip;