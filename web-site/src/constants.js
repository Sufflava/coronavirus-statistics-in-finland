export const STATE = { 
    INITIAL: "initial",
    SYNCHED: "synched"
};

// https://www.schemecolor.com/stunning-pie-chart-color-scheme.php
export const PALETTE = {
    COLOR_1: "#FFF1C9",
    COLOR_2: "#F7B7A3",
    COLOR_3: "#EA5F89",
    COLOR_4: "#9B3192",
    COLOR_5: "#57167E",
    COLOR_6: "#2B0B3F"
};

export const NUMBER_OF_TESTS_MEASURE_ID = "445356";
export const NUMBER_OF_POSITIVE_CASES_MEASURE_ID = "444833";
export const NUMBER_OF_DEATHS_MEASURE_ID = "492118";
export const POPULATION_MEASURE_ID = "445344";

export const MEASURE_LABEL_TRANSLATION_KEY_MAP = {
    [NUMBER_OF_TESTS_MEASURE_ID]: "statistics.numberOfTests",
    [NUMBER_OF_POSITIVE_CASES_MEASURE_ID]: "statistics.numberOfPositiveCases",
    [NUMBER_OF_DEATHS_MEASURE_ID]: "statistics.numberOfDeaths"
};

export const ALL_HOSPITALISATION_AREA_ID = 0;
export const HELSINKI_HOSPITALISATION_AREA_ID = 1;
export const KUOPIO_HOSPITALISATION_AREA_ID = 2;
export const OULU_HOSPITALISATION_AREA_ID = 3;
export const TAMPERE_HOSPITALISATION_AREA_ID = 4;
export const TURKU_HOSPITALISATION_AREA_ID = 5;

export const HOSPITALISATION_AREA_SHORT_LABEL_TRANSLATION_KEY_MAP = {
    [ALL_HOSPITALISATION_AREA_ID]: "statistics.nationwide",
    [HELSINKI_HOSPITALISATION_AREA_ID]: "geography.helsinki_",
    [KUOPIO_HOSPITALISATION_AREA_ID]: "geography.kuopio_",
    [OULU_HOSPITALISATION_AREA_ID]: "geography.oulu_",
    [TAMPERE_HOSPITALISATION_AREA_ID]: "geography.tampere_",
    [TURKU_HOSPITALISATION_AREA_ID]: "geography.turku_",
};

export const HOSPITALISATION_AREA_LABEL_TRANSLATION_KEY_MAP = {
    [ALL_HOSPITALISATION_AREA_ID]: "statistics.nationwideTotal",
    [HELSINKI_HOSPITALISATION_AREA_ID]: "geography.helsinki_university_area",
    [KUOPIO_HOSPITALISATION_AREA_ID]: "geography.kuopio_university_area",
    [OULU_HOSPITALISATION_AREA_ID]: "geography.oulu_university_area",
    [TAMPERE_HOSPITALISATION_AREA_ID]: "geography.tampere_university_area",
    [TURKU_HOSPITALISATION_AREA_ID]: "geography.turku_university_area",
};

export const ALL_AGES_MEASURE_ID = "444309";

export const ALL_SEXES_MEASURE_ID = "444328";
export const MEN_MEASURE_ID = "444356";
export const WOMEN_MEASURE_ID = "444563";

export const SEX_TRANSLATION_MAP = {
    [ALL_SEXES_MEASURE_ID]: "statistics.allSexes",
    [MEN_MEASURE_ID]: "statistics.men",
    [WOMEN_MEASURE_ID]: "statistics.women"
};

export const ALL_REGIONS_MEASURE_ID = "445222";
export const HUS_REGION_MEASURE_ID = "445193";
export const ALL_WEEKS_MEASURE_ID = "443686";

export const REGION_MEASURES_TO_GEO_AREA_MAP = {
    "444996": "hcd19",
    "445014": "hcd7",
    "445043": "hcd9",
    "445079": "hcd16",
    "445101": "hcd20",
    "445131": "hcd2",
    "445155": "hcd10",
    "445170": "hcd4",
    "445175": "hcd11",
    "445178": "hcd8",
    "445190": "hcd21",
    "445193": "hcd23",
    "445197": "hcd3",
    "445206": "hcd5",
    "445222": "hcd1", // all
    "445223": "hcd13",
    "445224": "hcd22",
    "445225": "hcd15",
    "445230": "hcd17",
    "445282": "hcd6",
    "445285": "hcd14",
    "445293": "hcd12"
};