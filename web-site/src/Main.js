import React from "react";
import { renderToStaticMarkup } from "react-dom/server";
import { withLocalize } from "react-localize-redux";
import enTranslations from "./translations/global.en.json";
import ruTranslations from "./translations/global.ru.json";
import Layout from './Layout';

const LANGUAGE_CODE = {
    en: "en",
    ru: "ru"
};

const languages = [
    { name: "EN", code: LANGUAGE_CODE.en },
    { name: "RU", code: LANGUAGE_CODE.ru }
];

class Main extends React.Component {
    constructor(props) {
        super(props);
        this._initTranslations();
    }

    _initTranslations = () => {
        this.props.initialize({
            languages,
            options: {
                renderToStaticMarkup,
                renderInnerHtml: true,
                defaultLanguage: LANGUAGE_CODE.en,
                activeLanguage: LANGUAGE_CODE.en
            }
        });

        this.props.addTranslationForLanguage(enTranslations, LANGUAGE_CODE.en);
        this.props.addTranslationForLanguage(ruTranslations, LANGUAGE_CODE.ru);
    }

    render() {
        return (
            <Layout/>
        );
    }
}

export default withLocalize(Main);
