// For a given year generates an object 
// {year: date[]}, date: { weekNumber, date_of_sunday }
// Result of work of this function can be found in file last-day-of-week.json
function generateLastDaysOfWeeksByYear(year) {
  var dates = [];
  var weeksCount = 53;
  
  Array.from(Array(weeksCount).keys()).forEach(i => {
    var weekNumber = i + 1;
    var date = moment().year(year).week(weekNumber).add(5, "day").format("YYYY-MM-DD");
    var key = weekNumber < 10 ? "0" + weekNumber : "" + weekNumber;
    dates.push({[key]: date})
  });
  
  return {["" + year]: dates};
}

// weekLabel is in format "Year 2020 Week 01" (as it comes from THL's Api)
// function returns year and week number: {year: "2020", week: "01"}
function parseWeekLabel(weekLabel) {
  //var weekLabel = "Year 2020 Week 01";
  var r = /Year (?<year>[0-9]{4}) Week (?<week>[0-9]{2})/;
  var groups = weekLabel.match(r).groups;
  //console.log(groups.year); // 2020
  //console.log(groups.week); // 01
  //console.log(moment().year(groups.year).week(groups.week).format("YYYY-MM-DD"))
  // 2014-25-23

  return {year: groups.year, week: groups.week};
}

// requires jsonstat-toolkit
function generateLabels() {
  var dailyDataUrl = "//bitbucket.org/Sufflava/coronavirus-statistics-in-finland/raw/19de733db59c1e6e9756d19fc2319cc60fc7331b/data/all-measures-daily.json";
  var weeklyDataUrl = "//bitbucket.org/Sufflava/coronavirus-statistics-in-finland/raw/8b66a73bdb00926c4ad448852510d88dfd69e1c2/data/all-measures-weekly.json";
  var regionsDataUrl = "//bitbucket.org/Sufflava/coronavirus-statistics-in-finland/raw/6c4bd018d2eda3057e59f2f51cb8ac80f9d3bc6f/data/number-of-cases-by-region-weekly-fi.json";
  var agesDataUrl = "//bitbucket.org/Sufflava/coronavirus-statistics-in-finland/raw/6c4bd018d2eda3057e59f2f51cb8ac80f9d3bc6f/data/number-of-cases-by-age-all.json";
  var sexDataUrl = "//bitbucket.org/Sufflava/coronavirus-statistics-in-finland/raw/6c4bd018d2eda3057e59f2f51cb8ac80f9d3bc6f/data/number-of-deaths-by-sex-weekly.json";

  var labels = {};

  JSONstat(dailyDataUrl).then(function(J) { 
    labels.days = J.Dataset(0).__tree__.dimension["dateweek2020010120201231"].category.label;
  });

  JSONstat(weeklyDataUrl).then(function(J) { 
    labels.weeks = J.Dataset(0).__tree__.dimension["dateweek2020010120201231"].category.label;
    labels.measures = J.Dataset(0).__tree__.dimension.measure.category.label;
  }

  JSONstat(regionsDataUrl).then(function(J) {  
    labels.regions = J.Dataset(0).__tree__.dimension["hcdmunicipality2020"].category.label;
  });

  JSONstat(agesDataUrl).then(function(J) {  
    labels.ages = J.Dataset(0).__tree__.dimension["ttr10yage"].category.label;
  });

  JSONstat(sexDataUrl).then(function(J) {  
    labels.sex = J.Dataset(0).__tree__.dimension["sex"].category.label;
  });

  /*
    The structure of the file:
    {
      days: {id: date}, // id of day, date is in format "2020-02-25"
      weeks: {id: weekNumber}, // id of week, weekNumber is in format "Year 2020 Week 28" (there is also item with label "Time")
      measures: {id: nameEn}, // id of measure, nameEn is label in english
      regions: {id: nameFi}, // id of region (kunta), nameFi is label in Finnish
      ages: {id, ageGroup}, // id of age group, ageGroup is in format "00-10", (there is also item with label "All ages")
      sex: {id, sexNameEn}, // id, sexNameEn is one of: "All sexes", "Men", Women"
    }
  */
}

function onDownloadBtnClick() {
  var data = getData();
  var json = JSON.stringify(data);
  downloadJson(json, "data.json")
}

function getData() {
  return {
    items: [
      {id: 1, name: "name 1"},
      {id: 2, name: "name 2"},
      {id: 3, name: "name 3"},
      {id: 4, name: "name 4"},
      {id: 5, name: "name 5"}
    ]
  };
}

function downloadJson(jsonObject, fileName) {
  var blob = new Blob([jsonObject], {type: "text/plain;charset=utf-8"});
  var url = window.URL || window.webkitURL;
  var link = url.createObjectURL(blob);
  var a = $("<a />");
  a.attr("download", fileName);
  a.attr("href", link);
  $("body").append(a);
  a[0].click();
  $("body").remove(a);
}

function groupBy(array, key) {
  return array.reduce(
    (result, item) => ({
      ...result,
      [item[key]]: [
        ...(result[item[key]] || []),
        item,
      ],
    }), 
    {},
  );
}